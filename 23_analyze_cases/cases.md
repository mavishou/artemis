# Command examples
## Get content for visualization
```bash
# Run on HPC

# Overall run python
python visualize.py -d LUNAR1 -l NR_126487.1 ENSG00000140443.13 -c T-ALL T_CELL

# For customized region and cell line, run this shell script.
# If you want to change the sample, edit the *.sample file, then run this script
bash visualize.sh chr1:47091653-47237220 K562 visualize/ncRNA-a3 "ENSG00000225506.2 ENSG00000162367.11"

# get the result back to Solar
rsync -arzvPR ncRNA-a3 houm@202.205.131.15:/lustre/user/houm/projects/Artemis/23_analyze_cases/visualize/
```

## Generate batch file for IGV
```bash
# Run on local
python generate_IGV_batch_file.py -l LUNAR1 -c T-ALL T_Cell
```

# Validated cases
## LUNAR1
```bash
# Solar, check the rank
cat all_sig_compare_result.txt | grep NR_126487.1 | grep ENSG00000140443.13 | cut -f 1,2,3,4,31
GM12878_vs_T-ALL        up      NR_126487.1     ENSG00000140443.13      12
HCT116_vs_HeLa-S3       up      NR_126487.1     ENSG00000140443.13      595
HCT116_vs_T-ALL up      NR_126487.1     ENSG00000140443.13      175
HeLa-S3_vs_HUVEC        down    NR_126487.1     ENSG00000140443.13      290
HUVEC_vs_T-ALL  up      NR_126487.1     ENSG00000140443.13      58
K562_vs_T-ALL   up      NR_126487.1     ENSG00000140443.13      8
T-ALL_vs_T_Cell down    NR_126487.1     ENSG00000140443.13      3

# HPC, for visualization
hpc_run "python visualize.py -d LUNAR1 -l NR_126487.1 ENSG00000140443.13"

# Local, for IGV
python generate_IGV_batch_file.py -l LUNAR1 -c T-ALL T_Cell
```

## HOTTIP
```bash
# Solar, check the rank
cd ../22_pipeline
cat all_sig_compare_result.txt | grep ENSG00000243766.7 | grep -f ../23_analyze_cases/HOXA_ENSG.txt | cut -f 1,2,3,4,31 | awk '$5<=20'
BJ_vs_H1        down    ENSG00000243766.7       ENSG00000253293.4       14
BJ_vs_IMR90     down    ENSG00000243766.7       ENSG00000253293.4       3
BJ_vs_IMR90     down    ENSG00000243766.7       ENSG00000005073.5       4
BJ_vs_IMR90     down    ENSG00000243766.7       ENSG00000078399.15      11
BJ_vs_K562      down    ENSG00000243766.7       ENSG00000005073.5       11
BJ_vs_K562      down    ENSG00000243766.7       ENSG00000253293.4       13
BJ_vs_K562      down    ENSG00000243766.7       ENSG00000078399.15      17

# HPC, for visualization
# get HOXA genes
cat ../01_gene_model/JS_anno_maps.txt | grep HOXA | cut -f 1,2 | awk '$2~/^HOXA[0-9]+$/' | sort | uniq > HOXA_genes.txt
cat HOXA_genes.txt | cut -f 1 > HOXA_ENSG.txt
# run
hpc_run "python visualize.py -d HOTTIP -l ENSG00000243766.7 ENSG00000005073.5 ENSG00000078399.15 ENSG00000105991.7 ENSG00000105996.6 ENSG00000105997.22 ENSG00000106004.4 ENSG00000106006.6 ENSG00000106031.7 ENSG00000122592.7 ENSG00000197576.13 ENSG00000253293.4"

# Local, for IGV
python generate_IGV_batch_file.py -l HOTTIP -c BJ IMR90

cat final/to_report.txt | grep ENSG00000243766.7 | grep -f ../23_analyze_cases/HOXA_ENSG.txt | sort -nk7,7
```

## CCAT1-L
```bash
# Solar, check the rank
cat all_sig_compare_result.txt | grep ENSG00000247844.1  | grep ENSG00000136997.14 | cut -f 1,2,3,4,31 | awk '$5<=50'
BJ_vs_HeLa-S3   up      ENSG00000247844.1       ENSG00000136997.14      7
GM12878_vs_HeLa-S3      up      ENSG00000247844.1       ENSG00000136997.14      7
H1_vs_HeLa-S3   up      ENSG00000247844.1       ENSG00000136997.14      11
HeLa-S3_vs_IMR90        down    ENSG00000247844.1       ENSG00000136997.14      47
HeLa-S3_vs_T-ALL        down    ENSG00000247844.1       ENSG00000136997.14      8
HeLa-S3_vs_T_Cell       down    ENSG00000247844.1       ENSG00000136997.14      6

# HPC, for visualization
hpc_run "python visualize.py -d CCAT1 -l ENSG00000247844.1 ENSG00000136997.14"

# Local, for IGV
python generate_IGV_batch_file.py -l CCAT1 -c HeLa-S3 GM12878
# more cell lines
python generate_IGV_batch_file.py -l CCAT1 -c HeLa-S3 HCT116 H1 GM12878 -n batch_more.txt

# to report
cat to_report.txt | awk '$8==1' | grep ENSG00000247844.1  | grep ENSG00000136997.14 | sort -nk7,7
```

## ncRNA-a3
```bash
# Solar, check the rank
cat all_sig_compare_result.txt | grep ENSG00000225506.2 | grep ENSG00000162367.11 | cut -f 1,2,3,4,31
GM12878_vs_K562 up      ENSG00000225506.2       ENSG00000162367.11      451
IMR90_vs_K562   up      ENSG00000225506.2       ENSG00000162367.11      212
K562_vs_T-ALL   down    ENSG00000225506.2       ENSG00000162367.11      960
K562_vs_T_Cell  down    ENSG00000225506.2       ENSG00000162367.11      199

# HPC, for visualization
hpc_run "python visualize.py -d ncRNA-a3 -l ENSG00000225506.2 ENSG00000162367.11"

# Local, for IGV
python generate_IGV_batch_file.py -l ncRNA-a3 -c K562 MCF-7 IMR90
python generate_IGV_batch_file.py -l ncRNA-a3 -c K562 MCF-7 IMR90 GM12878 -n batch_more.txt
```

# Other cis cases
For these cases, lncRNAs are validated to regulate cis, but not sure whether by looping.

## HOTAIRM1
Rank is good, but not support by Hi-C.

```bash
# Solar, check the rank
cat all_sig_compare_result.txt | grep ENSG00000233429.9 | grep ENSG00000197576.13 | cut -f 1,2,3,4,31 | awk '$5<=500'
BJ_vs_IMR90     up      ENSG00000233429.9       ENSG00000197576.13      115
GM12878_vs_HUVEC        up      ENSG00000233429.9       ENSG00000197576.13      402
H1_vs_HUVEC     up      ENSG00000233429.9       ENSG00000197576.13      311
H1_vs_IMR90     up      ENSG00000233429.9       ENSG00000197576.13      126
H1_vs_NHEK      up      ENSG00000233429.9       ENSG00000197576.13      364
HUVEC_vs_K562   down    ENSG00000233429.9       ENSG00000197576.13      249
HUVEC_vs_T-ALL  down    ENSG00000233429.9       ENSG00000197576.13      203
HUVEC_vs_T_Cell down    ENSG00000233429.9       ENSG00000197576.13      441

hpc_run "python visualize.py -d HOTAIRM1 -l ENSG00000233429.9 ENSG00000005020.12 ENSG00000197576.13"
```

```bash
compare with the HOXA locus
cat all_sig_compare_result.txt | grep ENSG00000233429.9 | grep -f ../23_analyze_cases/HOXA_ENSG.txt | cut -f 1,2,3,4,31 | awk '$5<=50'
BJ_vs_IMR90     up      ENSG00000233429.9       ENSG00000106004.4       11
BJ_vs_NHEK      up      ENSG00000233429.9       ENSG00000106004.4       50
BJ_vs_T-ALL     down    ENSG00000233429.9       ENSG00000253293.4       45
GM12878_vs_IMR90        up      ENSG00000233429.9       ENSG00000106004.4       36
H1_vs_HUVEC     up      ENSG00000233429.9       ENSG00000122592.7       36
H1_vs_HUVEC     up      ENSG00000233429.9       ENSG00000078399.15      49
H1_vs_IMR90     up      ENSG00000233429.9       ENSG00000106004.4       12
H1_vs_NHEK      up      ENSG00000233429.9       ENSG00000253293.4       21
H1_vs_NHEK      up      ENSG00000233429.9       ENSG00000078399.15      24
H1_vs_NHEK      up      ENSG00000233429.9       ENSG00000122592.7       27
HCT116_vs_IMR90 up      ENSG00000233429.9       ENSG00000106004.4       19
HCT116_vs_K562  down    ENSG00000233429.9       ENSG00000078399.15      11
HCT116_vs_K562  down    ENSG00000233429.9       ENSG00000253293.4       13
HCT116_vs_K562  down    ENSG00000233429.9       ENSG00000122592.7       25
HCT116_vs_K562  down    ENSG00000233429.9       ENSG00000106006.6       49
HeLa-S3_vs_IMR90        up      ENSG00000233429.9       ENSG00000106004.4       37
IMR90_vs_K562   down    ENSG00000233429.9       ENSG00000106004.4       13
IMR90_vs_MCF-7  down    ENSG00000233429.9       ENSG00000106004.4       37
IMR90_vs_T-ALL  down    ENSG00000233429.9       ENSG00000106004.4       10
IMR90_vs_T_Cell down    ENSG00000233429.9       ENSG00000106004.4       7
MCF-7_vs_NHEK   up      ENSG00000233429.9       ENSG00000078399.15      15
NHEK_vs_T-ALL   down    ENSG00000233429.9       ENSG00000078399.15      37
NHEK_vs_T-ALL   down    ENSG00000233429.9       ENSG00000253293.4       40
NHEK_vs_T-ALL   down    ENSG00000233429.9       ENSG00000122592.7       50
```

```bash
cat final/to_report.txt | grep ENSG00000233429.9  | grep -f ../23_analyze_cases/HOXA_ENSG.txt | awk '$8==1' | sort -nk7,7
```

## ncRNA-a2
The rank is low.

```bash
# Solar, check the rank
cat all_sig_compare_result.txt | grep ENSG00000228288.6 | grep ENSG00000117153.15 | cut -f 1,2,3,4,31
H1_vs_T-ALL     down    ENSG00000228288.6       ENSG00000117153.15      2601
HCT116_vs_T-ALL down    ENSG00000228288.6       ENSG00000117153.15      25064
K562_vs_T-ALL   down    ENSG00000228288.6       ENSG00000117153.15      7127
K562_vs_T_Cell  down    ENSG00000228288.6       ENSG00000117153.15      24473
MCF-7_vs_T-ALL  down    ENSG00000228288.6       ENSG00000117153.15      2941
MCF-7_vs_T_Cell down    ENSG00000228288.6       ENSG00000117153.15      8182
```
## ncRNA-a4
The rank is low.

```bash
# Solar, check the rank
cat all_sig_compare_result.txt | grep ENSG00000224805.2 | grep ENSG00000162368.13 | cut -f 1,2,3,4,31
BJ_vs_K562      up      ENSG00000224805.2       ENSG00000162368.13      8036
HeLa-S3_vs_K562 up      ENSG00000224805.2       ENSG00000162368.13      11906
HUVEC_vs_K562   up      ENSG00000224805.2       ENSG00000162368.13      9837
IMR90_vs_K562   up      ENSG00000224805.2       ENSG00000162368.13      4915
K562_vs_MCF-7   down    ENSG00000224805.2       ENSG00000162368.13      6993
K562_vs_NHEK    down    ENSG00000224805.2       ENSG00000162368.13      3421
K562_vs_T-ALL   down    ENSG00000224805.2       ENSG00000162368.13      5409
K562_vs_T_Cell  down    ENSG00000224805.2       ENSG00000162368.13      1592
```

## ncRNA-a5
The rank is low.

```bash
# Solar, check the rank
cat all_sig_compare_result.txt | grep ENSG00000224177.6 | grep ENSG00000134318.13 | cut -f 1,2,3,4,31
GM12878_vs_HeLa-S3      up      ENSG00000224177.6       ENSG00000134318.13      5713
HeLa-S3_vs_K562 down    ENSG00000224177.6       ENSG00000134318.13      19693
HeLa-S3_vs_NHEK down    ENSG00000224177.6       ENSG00000134318.13      5412
HeLa-S3_vs_T-ALL        down    ENSG00000224177.6       ENSG00000134318.13      13851
HeLa-S3_vs_T_Cell       down    ENSG00000224177.6       ENSG00000134318.13      10146
```


