#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2017/3/10

import argparse
from myToolKit.general_helper import *
from myToolKit.bio.gtf.gtf_util import processGTFAnno
from myToolKit.util.decorators import print_both_single_line


def get_region_and_gene_model(gene_list, project_dir, gene_dir):
    all_gene_bed_file = os.path.join(project_dir, '01_gene_model', 'artemis_gene.bed')
    all_exon_gtf_file = os.path.join(project_dir, '01_gene_model', 'artemis_exon.gtf')
    out_gene_bed_file = os.path.join(gene_dir, 'gene.bed')
    out_exon_gtf_file = os.path.join(gene_dir, 'exon.gtf')
    coords = []
    chrs = set()

    with open(all_gene_bed_file) as reader, open(out_gene_bed_file, 'w') as writer:
        for line in reader:
            cols = line_to_list(line)
            gene_id = cols[3]
            if gene_id in gene_list:
                chrs.add(cols[0])
                coords += [int(cols[1]) + 1, int(cols[2])]
                writer.write(line)

    if len(chrs) > 1:
        raise Exception('These genes are on multiple chrs: {}'.format(chrs))
    else:
        chr = chrs.pop()

    start = min(coords)
    end = max(coords)
    logging.info('Original region: {}:{}-{}'.format(chr, start, end))

    expanded_base = 5000
    ex_start  = start - expanded_base
    if ex_start < 1:
        ex_start = 1
    ex_end = end + expanded_base
    expanded_region = '{}:{}-{}'.format(chr, ex_start, ex_end)

    with open(all_exon_gtf_file) as reader, open(out_exon_gtf_file, 'w') as writer:
        for line in reader:
            cols = line_to_list(line)
            gene_id = processGTFAnno(cols[8])['gene_id']
            if gene_id in gene_list:
                writer.write(line)

    return expanded_region


@print_both_single_line
def get_features_for_cell_type(region, cell_type, gene_dir, genes):
    logging.info('Run for {} in {}'.format(region, cell_type))
    cmd = 'bash visualize.sh {} {} {} "{}"'.format(region, cell_type, gene_dir, genes)
    logging.info(cmd)
    os.system(cmd)


def main(gene_list, cell_types, gene_dir, project_dir):
    prepare_dir(gene_dir)
    region = get_region_and_gene_model(gene_list, project_dir, gene_dir)

    for cell_type in cell_types:
        get_features_for_cell_type(region, cell_type, gene_dir, ' '.join(gene_list))


def get_all_cell_types(project_dir):
    cell_types = []
    cell_type_file = os.path.join(project_dir, '00_samples', 'all_cell_lines.txt')
    with open(cell_type_file) as reader:
        for line in reader.readlines():
            cell_types.append(line.rstrip('\n'))
    return cell_types


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--id', required=True, help='LUNAR1')
    parser.add_argument('-l', '--gene-list', nargs='+', required=True, help='e,g. NR_126487.1 ENSG00000140443.13')
    parser.add_argument('-c', '--cell-types', nargs='+', help='e.g. T-ALL T_Cell')
    parser.add_argument('-t', '--target_dir', default='visualize')
    parser.add_argument('-p', '--project-dir', default='/home/gaog_pkuhpc/users/houm/Artemis')

    args = parser.parse_args()

    all_cell_types = get_all_cell_types(args.project_dir)
    if args.cell_types is None:
        args.cell_types = all_cell_types
    else:
        for cell_type in args.cell_types:
            if cell_type not in all_cell_types:
                raise Exception('{} is not valid!'.format(cell_type))

    logging.info('Gene list: {}'.format(args.gene_list))
    logging.info('Cell types: {}'.format(args.cell_types))
    args.gene_dir = os.path.join(args.target_dir, args.id)
    main(args.gene_list, args.cell_types, args.gene_dir, args.project_dir)
