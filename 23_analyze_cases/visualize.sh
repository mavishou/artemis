#!/usr/bin/env bash

set -o errexit
set -o nounset
source ~/users/houm/bin/general_func.sh

REGION=$1
CELL_TYPE=$2
GENE_DIR=$3
GENE_LIST=$4
if [[ "$GENE_LIST" != "NO" ]]; then
    echo $GENE_LIST > $GENE_DIR/gene_list.txt
fi

# PROJECT_DIR=/home/gaog_pkuhpc/users/houm/Artemis
echo $REGION > $GENE_DIR/region.txt

_get_rna_sample() {
    local rna_sample_file=$CELL_DIR/RNA-Seq.sample
    if [[ ! -s $rna_sample_file ]]; then
        cat ../00_samples/RNA-Seq_final.txt | grep "$CELL_TYPE" | head -1 | cut -f 2 > $rna_sample_file
    fi
    SP_RNA=$(cat $rna_sample_file | head -1 | cut -f 1)
}

prepare_rna() {
    _get_rna_sample
    FULL_BAM=../07_RNA-Seq/alignment_hg38/$SP_RNA/$SP_RNA.sort.bam
    EXTRACT_BAM=$CELL_DIR/extract.$SP_RNA.bam
    EXP_COV_BDG=$CELL_DIR/exprs_coverage.$SP_RNA.bdg
    EXP_COV_BW=$CELL_DIR/$CELL_TYPE.exprs_coverage.bigWig
}


prepare() {    
    CELL_DIR=$GENE_DIR/$CELL_TYPE
    mkdir -p $CELL_DIR
    CHROM_SIZE_FILE=/lustre1/gaog_pkuhpc/users/houm/genome/human/hg38/hg38.chrom.sizes
}

_extract_exprs_reads() {
    local cmd="~/users/houm/tools/samtools-1.3.1/bin/samtools view -b -h $FULL_BAM $REGION > $EXTRACT_BAM"
    run_cmd "$cmd" "Extracting reads"
}

__cal_scale_factor() {
    local total_reads=$(cat ../07_RNA-Seq/stat_matched_reads.txt | grep "$SP_RNA" | cut -f 2)
    RNA_SCLALE_FACTOR=$(echo $total_reads | awk '{printf("%.16f", 1000000/$1)}')
    echo "scale factor: $RNA_SCLALE_FACTOR"
}

_get_exprs_coverage() {
    __cal_scale_factor
    local cmd="/lustre1/bioapps/bin/genomeCoverageBed -ibam $EXTRACT_BAM -bg -split -scale $RNA_SCLALE_FACTOR -g $CHROM_SIZE_FILE > $EXP_COV_BDG"
    run_cmd "$cmd" "Getting RNA-Seq coverage"
}

sort_bedGraph() {
    local in_file=$1
    local out_file=$2
    local cmd="cat $in_file | sort -k1,1 -k2,2n > $out_file"
    run_cmd "$cmd" "Sorting $in_file"
}

bedGraph_to_bigWig() {
    local bd_file=$1
    local bd_sort_file=$bd_file.sort
    sort_bedGraph $bd_file $bd_sort_file

    local bw_file=$2
    local chrom_size_file=$CHROM_SIZE_FILE
    local cmd="~/users/houm/bin/bedGraphToBigWig $bd_sort_file $chrom_size_file $bw_file"
    run_cmd "$cmd" "Converting $bd_file to $bw_file"
}

_convert_bedGraph_to_bigWig() {
    bedGraph_to_bigWig $EXP_COV_BDG $EXP_COV_BW
}


extract_exprs_cov() {
    _extract_exprs_reads
    _get_exprs_coverage
    _convert_bedGraph_to_bigWig
}


_get_chip_sample() {
    local chip_sample_file=$CELL_DIR/$CHIP_TP.sample
    if [[ ! -s $chip_sample_file ]]; then
        cat ../00_samples/ChIP-Seq_final.txt | grep "$CELL_TYPE" | grep "$CHIP_TP" | head -1 | cut -f 3 > $chip_sample_file
    fi
    SP_CHIP=$(cat $chip_sample_file | head -1 | cut -f 1)
}

prepare_chip() {
    _get_chip_sample
    FULL_SIGNAL_BW=../06_ChIP-Seq/call_peak/$SP_CHIP/${SP_CHIP}_pvalue.bigWig
    SIGNAL_BW=$CELL_DIR/$CELL_TYPE.${CHIP_TP}.pvalue.${SP_CHIP}.bigWig
    FULL_CHIP_BED=../06_ChIP-Seq/call_peak/$SP_CHIP/overlapped.bed.sorted.gz
    CHIP_PEAK_BED=$CELL_DIR/$CELL_TYPE.${CHIP_TP}.bed
}

extract_bigWig() {
    local from_bw=$1
    local to_bw=$2
    local region=$3
    local chr=$(echo $region | sed -r 's/^(chr.+):.+/\1/')
    local start=$(echo $region | sed -r 's/^chr.+:([0-9]+)-[0-9]+/\1/')
    local end=$(echo $region | sed -r 's/^chr.+:[0-9]+-([0-9]+)/\1/')
    
    local tmp_wig_file=$to_bw.wig
    local cmd_1="~/users/houm/bin/bigWigToWig $from_bw $tmp_wig_file -chrom=$chr -start=$start -end=$end"
    run_cmd "$cmd_1" "Extracting $tmp_wig_file from $from_bw"

    local cmd_2="~/users/houm/bin/wigToBigWig $tmp_wig_file $CHROM_SIZE_FILE $to_bw"
    run_cmd "$cmd_2" "Converting $tmp_wig_file to $to_bw"
    rm -f $tmp_wig_file
}

extract_chip_signal() {
    extract_bigWig $FULL_SIGNAL_BW $SIGNAL_BW $REGION
}

extract_chip_peak() {
    local cmd="/usr/local/bin/tabix $FULL_CHIP_BED $REGION > $CHIP_PEAK_BED"
    run_cmd "$cmd" "Extracting ChIP-Seq peaks from $FULL_CHIP_BED"
}

extract_chip() {
    local tp=$1
    CHIP_TP=$tp
    prepare_chip
    extract_chip_signal
    extract_chip_peak
}

prepare_hic() {
    FULL_HIC_GENE_GTF=../05_hic_loops/for_visualize/${CELL_TYPE}_loop_gene.gtf.sorted.gz
    FULL_HIC_EPAK_GTF_SORT=../05_hic_loops/for_visualize/${CELL_TYPE}_loop_peaks.gtf.sorted.gz
    HIC_PEAK_GENE=$CELL_DIR/$CELL_TYPE.HiC_peak.gene
    HIC_PEAK_GTF_REGION=$CELL_DIR/$CELL_TYPE.HiC_peak_region.gtf
    FULL_HIC_PEAK_GTF=../05_hic_loops/for_visualize/${CELL_TYPE}_loop_peaks.gtf
    HIC_PEAK_GTF=$CELL_DIR/$CELL_TYPE.HiC_peak.gtf
}

_get_hic_peak_gene() {
    echo "Extracting Hi-C peak gene from $FULL_HIC_GENE_GTF"
    # echo /usr/local/bin/tabix $FULL_HIC_GENE_GTF $REGION | awk '{print $10}' | sed -e 's/"//g' -e 's/;//' > $HIC_PEAK_GENE
    /usr/local/bin/tabix $FULL_HIC_GENE_GTF $REGION | awk '{print $10}' | sed -e 's/"//g' -e 's/;//' > $HIC_PEAK_GENE
}

_extract_hic_peaks_in_region() {
    local cmd="tabix $FULL_HIC_EPAK_GTF_SORT $REGION > $HIC_PEAK_GTF_REGION"
    run_cmd "$cmd" "Extracting Hi-C peaks from $FULL_HIC_EPAK_GTF_SORT in $REGION"
}


_get_hic_peaks() {
    local cmd="grep -f $HIC_PEAK_GENE -w $FULL_HIC_PEAK_GTF > $HIC_PEAK_GTF"
    run_cmd "$cmd" "Getting HiC peaks"
}

extract_hic_peak() {
    if [[ -s $FULL_HIC_GENE_GTF ]]; then
        _get_hic_peak_gene
        _extract_hic_peaks_in_region
    else
        echo "No Hi-C data for $CELL_TYPE."
    fi

    if [[ -s $HIC_PEAK_GENE ]]; then
        _get_hic_peaks
    else
        echo "No Hi-C genes in $HIC_PEAK_GENE."
    fi
}

remove_tmp_files() {
    cd $CELL_DIR
    rm -f *.bam *bdg* 
}

main() {
    echo $REGION
    echo $CELL_TYPE    
    prepare
    
    prepare_rna
    extract_exprs_cov

    extract_chip H3K4me3
    extract_chip H3K27ac

    prepare_hic
    extract_hic_peak

    remove_tmp_files
    
    echo "Done!"
}

main
