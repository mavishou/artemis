#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2017/3/14

# Run at local

import argparse
from myToolKit.general_helper import *
from myToolKit.util.decorators import print_func_name
from glob import glob


def check_file_exist(f):
    if not os.path.exists(f):
        logging.error('File {} not exist!'.format(f))
        raise Exception('File {} not exist!'.format(f))


class IgvBatch(object):

    def __init__(self, lnc_dir, cell_types, project_dir, batch_file):
        self.lnc_dir = lnc_dir
        self.cell_types = cell_types
        self.project_dir = project_dir
        self.batch_file = batch_file

        self.all_hic_cell_types = set()
        self._get_all_hic_cell_types()

        self.writer = open(self.batch_file, 'w')
        self._start_a_new_session()

        self.files_to_load = []
        self.tracks_to_squish = []

    def _get_all_hic_cell_types(self):
        hic_cell_type_file = os.path.join(self.project_dir, '00_samples', 'HiC_cell_lines.txt')
        with open(hic_cell_type_file) as reader:
            for line in reader:
                self.all_hic_cell_types.add(line.rstrip('\n'))

    def _start_a_new_session(self):
        self.writer.write('new\n')

    def _get_a_file_to_load(self, file_name):
        check_file_exist(file_name)
        self.files_to_load.append(file_name)

    def load_files(self):
        self.writer.write('load {}\n'.format(','.join(self.files_to_load)))

    @print_func_name
    def go_to_region(self):
        region_file = os.path.join(self.lnc_dir, 'region.txt')
        with open(region_file) as reader:
            region = reader.readline().rstrip('\n')
        self.writer.write('goto {}\n'.format(region))

    @print_func_name
    def get_gene_model_to_load(self):
        gene_bed = os.path.join(self.lnc_dir, 'gene.bed')
        exon_gtf = os.path.join(self.lnc_dir, 'exon.gtf')
        self._get_a_file_to_load(gene_bed)
        self._get_a_file_to_load(exon_gtf)

    @print_func_name
    def get_exprs_to_load(self):
        for cell_type in self.cell_types:
            exprs_file = os.path.join(self.lnc_dir, cell_type, '{}.exprs_coverage.bigWig'.format(cell_type))
            self._get_a_file_to_load(exprs_file)

    @print_func_name
    def get_chip_to_load(self, histone):
        for cell_type in self.cell_types:
            signal_file = glob(os.path.join(self.lnc_dir, cell_type, '{}.{}.pvalue.*.bigWig'.format(cell_type, histone)))[0]
            peak_file = os.path.join(self.lnc_dir, cell_type, '{}.{}.bed'.format(cell_type, histone))
            self._get_a_file_to_load(signal_file)
            self._get_a_file_to_load(peak_file)

    def _get_a_track_to_squish(self, full_path):
        track_name = os.path.basename(full_path)
        self.tracks_to_squish.append(track_name)

    @print_func_name
    def squish_tracks(self):
        for track in self.tracks_to_squish:
            self.writer.write('squish {}\n'.format(track))

    @print_func_name
    def get_hic_to_load(self):
        for cell_type in self.cell_types:
            if cell_type in self.all_hic_cell_types:
                hic_file = os.path.join(self.lnc_dir, cell_type, '{}.HiC_peak.gtf'.format(cell_type))
                if os.path.exists(hic_file):
                    self._get_a_file_to_load(hic_file)
                    self._get_a_track_to_squish(hic_file)
                else:
                    logging.info('{} has no Hi-C peaks in this region.'.format(cell_type))
            else:
                logging.info('{} has no Hi-C data.'.format(cell_type))


def main(lnc_dir, cell_types, project_dir, batch_file):
    igv_batch = IgvBatch(lnc_dir, cell_types, project_dir, batch_file)
    igv_batch.go_to_region()
    igv_batch.get_gene_model_to_load()
    igv_batch.get_exprs_to_load()
    igv_batch.get_chip_to_load('H3K4me3')
    igv_batch.get_chip_to_load('H3K27ac')
    igv_batch.get_hic_to_load()
    igv_batch.load_files()
    igv_batch.squish_tracks()
    logging.info('Done!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--lncRNA', required=True, help='LUNAR1')
    parser.add_argument('-c', '--cell-types', nargs='+', required=True, help='e.g. T-ALL T_Cell')
    parser.add_argument('-p', '--project-dir', default='/Users/Mavis/Projects/Artemis')
    parser.add_argument('-t', '--target-dir', default='23_analyze_cases/visualize')
    parser.add_argument('-n', '--batch-file-name', default='batch.txt')

    args = parser.parse_args()
    args.lnc_dir = os.path.join(args.project_dir, args.target_dir, args.lncRNA)
    args.batch_file = os.path.join(args.lnc_dir, args.batch_file_name)
    main(args.lnc_dir, args.cell_types, args.project_dir, args.batch_file)
