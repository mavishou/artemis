**Investigate LUNAR1**

[TOC]

# Prepare
From the manuscript: 
> we were able to clone a ==491==-nucleotide transcript containing 4 exons and a poly(A) tail (FASTA sequence in supplement).

RefSeq: [NR_126487](https://www.ncbi.nlm.nih.gov/nuccore/NR_126487.1). The sequence is ==484bp==. 

I'll use the one in RefSeq to do investigation.

1. Get the sequence from RefSeq as `lunar1.fa`.
2. Get the GTF

```bash
cat ../01_gene_model/JS_exon_full.gtf | grep LUNAR1 > lunar1_hg38.gtf
# Check length
cat lunar1_hg38.gtf | cal_gtf_len
# 484
```

# Look around
## AnnoLnc
See http://annolnc.cbi.pku.edu.cn/annoDetail.jsp?submitID=D1C4DAD0-161215053940&transID=27376&seqName=LUNAR.

No expression in any sample.

## LocExpress
See http://loc-express.cbi.pku.edu.cn/submit/mhSifkpBMobIUTdSGyqdsQ

## Expression
It's highly expressed in T cell acute lymphoblastic leukemia, but we don't have related samples or cell lines yet. From the result of LocExpress.

```bash
# get expression from JS
# cat ../02_expression/exp_from_js.txt | grep ^NR_126487.1 > lunar1_exp.txt
cd 02_expression
python extract_exp.py NR_126487.1 -o /lustre/user/houm/projects/Artemis/10_LUNAR1/lunar1_exp
python extract_exp.py ENSG00000140443.13 -g -o /lustre/user/houm/projects/Artemis/10_LUNAR1/IGF1R_exp
python extract_exp.py ENSG00000183571.9 -g -o /lustre/user/houm/projects/Artemis/10_LUNAR1/PGPEP1L_exp
```
## Correlation
Summary: correlation is low

### LUNAR1 vs. IGF1R
```bash
# pearson
cd 02_expression
python cal_corr.py -f1 ../10_LUNAR1/IGF1R_exp/exp.txt -f2 ../10_LUNAR1/lunar1_exp/exp.txt
        NR_126487.1
ENST00000268035.10      0.07365883883492834
ENST00000557873.5       0.047799642785498114
ENST00000557938.5       0.006418656640723303
ENST00000558355.1       0.040108500075239785
ENST00000558751.1       0.052315902886148305
ENST00000558762.5       0.08259690348201322
ENST00000558898.1       0.007075932274069697
ENST00000558947.1       0.004757643555673952
ENST00000559582.1       0.029962743285930942
ENST00000559925.5       0.005827679577739832
ENST00000560144.1       0.04668214251187513
ENST00000560186.5       0.004381370465375684
ENST00000560277.5       -0.00045017389169180275
ENST00000560343.1       0.05463476223750414
ENST00000560432.1       0.014051384585860301
ENST00000560972.1       0.0876357705917265
ENST00000561049.1       0.053643227120955636
ENSG00000140443.13      0.07849063733961695

# spearman
python cal_corr.py -f1 ../10_LUNAR1/IGF1R_exp/exp.txt -f2 ../10_LUNAR1/lunar1_exp/exp.txt -s
        NR_126487.1
ENST00000268035.10      0.02268805816299794
ENST00000557873.5       0.06678431296895034
ENST00000557938.5       0.07846931156186694
ENST00000558355.1       -0.026793402898630025
ENST00000558751.1       0.033207764155376955
ENST00000558762.5       0.06146493530212752
ENST00000558898.1       0.08965912613365305
ENST00000558947.1       0.07561042429945886
ENST00000559582.1       0.06305406756466171
ENST00000559925.5       0.04569641585615172
ENST00000560144.1       0.04606971554925683
ENST00000560186.5       0.07560425987121852
ENST00000560277.5       0.07410213485157353
ENST00000560343.1       0.06951126303589686
ENST00000560432.1       0.07135163230915362
ENST00000560972.1       0.08299289745015437
ENST00000561049.1       0.043295055530114895
ENSG00000140443.13      0.04002596195396942
```
### LUNAR1 vs. PGPEP1L
```bash
# spearman
houm@solar 02_expression $ python cal_corr.py -f1 ../10_LUNAR1/PGPEP1L_exp/exp.txt -f2 ../10_LUNAR1/lunar1_exp/exp.txt -s
        NR_126487.1
ENST00000378919.6       0.06339378243406518
ENST00000535714.1       0.15993487531743492
ENSG00000183571.9       0.11405034234639187

# pearson
houm@solar 02_expression $ python cal_corr.py -f1 ../10_LUNAR1/PGPEP1L_exp/exp.txt -f2 ../10_LUNAR1/lunar1_exp/exp.txt
        NR_126487.1
ENST00000378919.6       0.016108334481993062
ENST00000535714.1       0.06911334509480559
ENSG00000183571.9       0.027669750117921117
```

## Check the Hi-C data
### Get the GTF on hg19
```bash
liftOver -gff lunar1_hg38.gtf /lustre/user/houm/lift_over_chain/hg38ToHg19.over.chain lunar1_hg19.gtf unmapped
```

#### IGF1R
hg38:chr15:98648971-98964530
hg19:chr15:99192200-99507759

From IGF1R to LUNAR1 on hg19: chr15:99,192,200-99,574,279

> It's hard to inspect the region in Juicebox


# Visualize data based on original paper
Most datasets are based on hg19. So I do the investigation on hg19. See xxx for sample details

## Region
### Confirm LUNAR 1 region
#### hg38
UCSC: `chr15:99014704-99031046`
#### hg19
UCSC: `chr15:99557933-99574279`
AnnoLnc: `chr15:99557933-99574279` (Alignment the sequence in the original paper)
liftOver: `chr15:99557933-99574279` (from RefSeq annotation on hg38)
Original paper: `chr15:99557680-99585143`
**Use: `chr15:99557933-99574279`**
The region in original paper is different. But the region in UCSC, AnnoLnc and liftOver are the same.

### cis region to inspect
* **hg19: `chr15:99097229-99725324`**
* hg38: `chr15:98492191-99122010`


## Prepare GTF for hg19
```bash
cat /lustre/user/houm/projects/AnnoLnc/gene_model/gencode.v19.annotation.gtf | grep IGF1R > IGF1R_hg19.gtf
cat /lustre/user/houm/projects/AnnoLnc/gene_model/gencode.v19.annotation.gtf | grep PGPEP1L > PGPEP1L_hg19.gtf
cat IGF1R_hg19.gtf PGPEP1L_hg19.gtf lunar1_hg19.gtf > LUNAR1_cis.gtf
```

## RNA-Seq
[GSE57982](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE57982): generated by the original paper

```bash
# on HPC
cd 07_RNA-Seq
# fastq-dump example
hpc_run "fastq-dump --bzip2 --split-3 SRR1299472.sra"

# Run hisat2
cat ../10_LUNAR1/sample_RNA_Seq_0105.txt | hpc_batch -s do_alignment.sh -x HM-A -c 10
```
### Extract for visualzie
See `RNA-Seq/extract_expr_wig.sh`

## ChIP-Seq
Main histone marckers:

* H3K4me3: promoter
* H3K27ac: enhancer
* DNase-Seq: open state
* H3K4me1: promoter & enhancer compare
* H3K36me3: whole gene body

### T cell from ENCODE
[Search ENCODE](https://www.encodeproject.org/search/?type=Experiment&status=released&replicates.library.biosample.donor.organism.scientific_name=Homo+sapiens&y.limit=&replicates.library.biosample.biosample_type=primary+cell&biosample_term_name=T-cell&assay_title=ChIP-seq
)

### T-ALL from GEO
* CUTLL1 (used by the original paper)
	* [GSE29600](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE29600):  [Genome-wide analysis reveals conserved and divergent features of Notch1/RBPJ binding in human and murine T-lymphoblastic leukemia cells](http://www.pnas.org/content/108/36/14908.full) (2011)
	* [GSE51800](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE51800): [Long-range enhancer activity determines Myc sensitivity to Notch inhibitors in T cell leukemia](http://www.pnas.org/content/111/46/E4946.long) (2014)
* KOPT-K1 & DND-41
	* [GSE54379](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE54379): [An epigenetic mechanism of resistance to targeted therapy in T cell acute lymphoblastic leukemia](An epigenetic mechanism of resistance to targeted therapy in T cell acute lymphoblastic leukemia) (2013)

### Extract wig or bed within the cis region
cis files are started with cis-*

To extract wig:

```bash
# Example
T_H3K4me3.bigWig cis_T_H3K4me3.wig -chrom=chr15 -start=99097229 -end=99725324
# A script
bash extract_wig.sh T_H3K27ac.bigWig
# The result is cis_T_H3K27ac.wig
```

To extract bed:

```bash
# Example
(cat T_H3K27ac_broad.bed | sort -k1,1 -k2,2n) | bgzip > T_H3K27ac_broad.bed.sorted.gz
tabix -p bed T_H3K27ac_broad.bed.sorted.gz
tabix T_H3K27ac_broad.bed.sorted.gz chr15:99097229-99725324 > cis_T_H3K27ac_broad.bed
# A script
bash extract_bed.sh T_H3K27ac_narrow.bed
# The result is cis_T_H3K27ac_narrow.bed
```

### Analysis
```bash
# cd 06_ChIP-Seq
# fastq-dump
cat ../10_LUNAR1/sample_ChIP_0105.txt | cut -f 2 | hpc_batch -s do_fastq_dump.sh -x HM-D -c 1
# alignment and filter
cat ../10_LUNAR1/sample_ChIP_0105.txt | hpc_batch -s do_alignment.sh -x HM-BA -c 10
# call peak
cat ../10_LUNAR1/sample_call_peak_0106.txt | hpc_batch -s call_peak.sh -x HM-CP -c 1

# ENCODE
cat ../10_LUNAR1/sample_chip_0107.txt | cut -f 2 | hpc_batch -s do_fastq_dump.sh -x HM-D -c 1
cat ../10_LUNAR1/sample_chip_0107.txt | hpc_batch -s do_alignment.sh -x HM-BA -c 10
cat ../10_LUNAR1/sample_call_peak_0107.txt | hpc_batch -s call_peak.sh -x HM-CP -c 1
```
### Extract for visualize
See `ChIP-Seq/extract_bed.sh`

## Hi-C loops
Data: 
T cell: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE69600
Thymus: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE58752

Method: http://aidenlab.org/commandlinetools/docs.html

> Not try the Hi-C data because we don't have Hi-C for T-ALL. The author says that the data is still not published now.


## Result
Compare different ENCODE ChIP-Seq results
![](media/14840542505812.jpg)


# Find the LUNAR1-IGF1R pair by the inference model
* LUNAR1: NR_126487.1
* IGF1R: ENSG00000140443.13

## Datasets
See `final_datasets.xlsx`.

* RNA-Seq datasets. 2 whole thymus extract vs. 2 primary T-ALL. 
* ChIP-Seq datasets. T cell related datasets are from ENCODE. CUTLL H3K4me3 and H3K27ac only have one replicate. So I split the fasta files to get two sub-samples.

## RNA-Seq analysis
### Alignment
```bash
cd 07_RNA-Seq
cat ../00_samples/LUNAR1_RNA-Seq_grouped_0120.txt | hpc_batch -s do_alignment_hg38.sh -x HM-HS -c 5
```
### Call differentially expressed genes
#### Count reads
```bash
# count reads
cd 07_RNA-Seq
cat ../00_samples/LUNAR1_RNA-Seq_grouped_0120.txt | cut -f 1 | hpc_batch -s do_featureCounts.sh -x HM-FA -c 5
```
#### DESeq2
* Script: `diff_exp/diff_exp_lunar1.R`
* Result: `diff_exp/diff_result.txt`

### Get lncRNA gene - PCG pairs which are both differentially expressed in the same manner
* Script: `diff_exp/analyze_diff_exp.R`
* Result: `diff_exp/diff_pairs.txt`

> Only 239 pairs selected from total 1093939 pairs.
> LUNAR1 - IGF1R are in the 239 pairs.

## ChIP-Seq analysis
### Split reads
```bash
cd ../00_samples
hpc_run -J HM-S -c 1 "python split_fasta.py fastq/SRR243557.fastq.bz2"
hpc_run -J HM-S -c 1 "python split_fasta.py fastq/SRR1019701.fastq.bz2"
```

### Alignment
```bash
cd 06_ChIP-Seq
cat ../00_samples/LUNAR1_ChIP-Seq_for_mapping.txt | hpc_batch -s do_alignment_hg38.sh -x HM-BT -c 10
```
### Call peak
```bash
cd 06_ChIP-Seq
cat ../00_samples/LUNAR1_ChIP-Seq_call_peak.txt | hpc_batch -s call_peak.sh -x HM-CP -c 1
```
### Get consensus peak regions

```bash
cd diff_peak
```

1. Filter peaks. Only use peaks with adjp <= 0.01

	```bash
	cat sp_H3K4me | while read s; do cat ../../06_ChIP-Seq/call_peak/$s/macs2_peaks.narrowPeak | awk '$9>=2' | cut -f 1,2,3,4,9 > $s.bed; done
	cat sp_H3K27ac | while read s; do cat ../../06_ChIP-Seq/call_peak/$s/macs2_peaks.narrowPeak | awk '$9>=2' | cut -f 1,2,3,4,9 > $s.bed; done
	```

2. Only use peaks that are overlapped with peaks in the other replicate file

	```bash
	# H3K4me3
	intersectBed -a GSM1058782_1.bed -b GSM1058782_2.bed -c | awk '$6>0' | cut -f 1,2,3,4,5 > GSM1058782_1_overlapped.bed
	intersectBed -a GSM1058782_2.bed -b GSM1058782_1.bed -c | awk '$6>0' | cut -f 1,2,3,4,5 > GSM1058782_2_overlapped.bed
	intersectBed -a GSM732911_1.bed -b GSM732911_2.bed -c | awk '$6>0' | cut -f 1,2,3,4,5 > GSM732911_1_overlapped.bed
	intersectBed -a GSM732911_2.bed -b GSM732911_1.bed -c | awk '$6>0' | cut -f 1,2,3,4,5 > GSM732911_2_overlapped.bed
	
	# H3K27ac
	intersectBed -a GSM1058764_1.bed -b GSM1058764_2.bed -c | awk '$6>0' | cut -f 1,2,3,4,5 > GSM1058764_1_overlapped.bed
	intersectBed -a GSM1058764_2.bed -b GSM1058764_1.bed -c | awk '$6>0' | cut -f 1,2,3,4,5 > GSM1058764_2_overlapped.bed
	intersectBed -a GSM1252938_1.bed -b GSM1252938_2.bed -c | awk '$6>0' | cut -f 1,2,3,4,5 > GSM1252938_1_overlapped.bed
	intersectBed -a GSM1252938_2.bed -b GSM1252938_1.bed -c | awk '$6>0' | cut -f 1,2,3,4,5 > GSM1252938_2_overlapped.bed
	```

3. Prepare bed files for merge
	Script: `prepare_bed_for_merge.R`

4. Merge peaks in the two conditions to get consensus regions

	```bash
	mergeBed -i H3K4me_for_merge.bed -c 4 -o collapse > H3K4me3_merged.txt
	mergeBed -i H3K27ac_for_merge.bed -c 4 -o collapse > H3K27ac_merged.txt
	```

5. Clean the merged file to get bed and saf format

	```bash
	# Get bed format 
	cat H3K4me3_merged.txt | awk '{print $1"\t"$2"\t"$3"\tH3K4me3_P"NR}' > H3K4me3_merged.bed
	cat H3K27ac_merged.txt | awk '{print $1"\t"$2"\t"$3"\tH3K27ac_P"NR}' > H3K27ac_merged.bed
	
	# Get SAF format for featureCounts
	cat H3K4me3_merged.txt | awk '{print "H3K4me3_P"NR"\t"$1"\t"$2"\t"$3"\t."}' > H3K4me3_merged.saf
	cat H3K27ac_merged.txt | awk '{print "H3K27ac_P"NR"\t"$1"\t"$2"\t"$3"\t."}' > H3K27ac_merged.saf
	```

### Call diff peaks
#### Count reads in consensus peak regions
```bash
cd 06_ChIP-Seq
# Remove duplicated reads
cat ../00_samples/LUNAR1_ChIP-Seq_call_peak.txt | cut -f 1 | hpc_batch -s remove_dup_reads.sh -x HM-DD -c 1

# In current directory
cat for_fc | hpc_batch -s do_featureCounts.sh -x HM-FC -c 5
```
#### DESeq2
* Script: `diff_peak_lunar1.R`
* Results:
	* `diff_result_H3K4me3.txt`
	* `diff_result_H3K27ac.txt`

## Combine diff peaks with diff expressed lnc - PCG pairs
1. Use `analyze_diff_peak.R` to clean the diff peak results

2. Get diff gene - diff peak pairs

	```bash
	cd diff_peak
	windowBed -a ../diff_exp/diff_genes.bed -b diff_peaks.bed -w 3000 > gene_neighbor_peaks.txt
	```

3. Combine them and calculate a score
	> The Score is calculated as `-log10(all p values)`. In this way, the LUNAR1-IGF1R pair 	get the rank 4. It's a very good result but here the p value of ChIP-Seq takes too much 
	weight. I'll try to develop a score system.
	
	* Script: `combine_exp_peak.R`
	* Result: `combine_score.txt`

### Develop an overall score system
* Total pair of lncRNA-PCG: 1093938
* Total expressed genes: 69543
* Expressed genes in this direction: 37920
* Expressed pairs (directed): 104400 
* Directed genes has diff peaks: 9438

```bash
cd diff_peak
windowBed -a ../diff_exp/all_diff_directed_genes.bed -b diff_peaks.bed -w 3000 > all_diff_directed_gene_neighbor_peaks.txt
```

* Script: `score.R`
* Result: `final_scores.txt`

**LUNAR1 - IGF1R pair hits top 2**

#### How does the score system work?
1. Get all diff exp p values for 69543 expressed genes, then get their rank, and fit them into [0, 1]
2. For each expressed genes, get peak p values. If no peak, p value = 1. If multiple p values, use the `minimump`. Then get the rank and fit into [0, 1]
3. Sum the 4 scores of each pair.


