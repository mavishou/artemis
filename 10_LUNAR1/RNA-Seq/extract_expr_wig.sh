#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1
PREFIX=$2
REGION=chr15:99097229-99725324

prepare_target_dir() {
    TARGET_DIR=$ID
    mkdir -p $TARGET_DIR
}

extract_reads() {
    ALN_DIR=../../07_RNA-Seq/alignment/$ID
    FULL_BAM=$ALN_DIR/$ID.sort.bam
    EXTRACT_BAM=$TARGET_DIR/extract.bam
    cmd="samtools view -b -h $FULL_BAM $REGION > $EXTRACT_BAM"
    run_cmd "$cmd" "Extracting reads"
}

_cal_scale_factor() {
    local total_reads=$(cat $ALN_DIR/total_mapped_reads)
    SCLALE_FACTOR=$(echo $total_reads | awk '{printf("%.16f", 1000000/$1)}')
    echo "scale factor: $SCLALE_FACTOR"
}

get_coverage() {
    _cal_scale_factor
    local chrom_size_file=/lustre1/gaog_pkuhpc/users/houm/genome/human/hg19/hg19.chrom.sizes
    COV_BDG=$TARGET_DIR/coverage.bdg
    local cmd="genomeCoverageBed -ibam $EXTRACT_BAM -bg -split -scale $SCLALE_FACTOR -g $chrom_size_file > $COV_BDG"
    run_cmd "$cmd" "Getting coverage"
}

_sort_bedGraph() {
    local in_file=$1
    local out_file=$2
    local cmd="cat $in_file | sort -k1,1 -k2,2n > $out_file"
    run_cmd "$cmd" "Sorting $in_file"
}

_bedGraph_to_bigWig() {
    local bd_file=$1
    local bd_sort_file=$bd_file.sort
    _sort_bedGraph $bd_file $bd_sort_file

    local bw_file=$2
    local chrom_size_file=/lustre1/gaog_pkuhpc/users/houm/genome/human/hg19/hg19.chrom.sizes
    local cmd="bedGraphToBigWig $bd_sort_file $chrom_size_file $bw_file"
    run_cmd "$cmd" "Converting $bd_file to $bw_file"
}

convert_bedGraph_to_bigWig() {
    COV_BW=${PREFIX}_coverage.bigWig
    _bedGraph_to_bigWig $COV_BDG $COV_BW
}

main() {
    echo $ID
    prepare_target_dir
    extract_reads
    get_coverage
    convert_bedGraph_to_bigWig
}

main
