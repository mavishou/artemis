#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1
PEAK_FILE=$2
CPU_NUM=5

prepare() {
    BAM_FILE=../06_ChIP-Seq/alignment_hg38/$ID/dedupped.sort.bam
    TARGET_DIR=diff_peak/fc/$ID
    mkdir -p $TARGET_DIR
}

make_fc_cmd() {
    CMD_FC="~/users/houm/tools/subread-1.5.1-Linux-x86_64/bin/featureCounts"
    #CMD_FC="$CMD_FC -p" # paired
    CMD_FC="$CMD_FC -T $CPU_NUM"
    #CMD_FC="$CMD_FC -t exon"
    #CMD_FC="$CMD_FC -g gene_id"
    CMD_FC="$CMD_FC -a $PEAK_FILE"
    CMD_FC="$CMD_FC -F SAF"
    #CMD_FC="$CMD_FC --ignoreDup"
    CMD_FC="$CMD_FC -o $TARGET_DIR/peak_reads_count"
    # CMD_FC="$CMD_FC -M" # count multiple re reads
    CMD_FC="$CMD_FC $BAM_FILE"
}

run_featureCounts() {
    make_fc_cmd
    run_cmd "$CMD_FC" "Running featureCounts"
}

main() {
    echo $ID
    prepare
    run_featureCounts
}

main
