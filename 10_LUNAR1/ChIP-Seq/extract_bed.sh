#!/usr/bin/env bash

BED_FILE=$1
REGION=chr15:99097229-99725324

prepare_for_tabix() {
    SORT_FILE=$BED_FILE.sorted.gz
    (cat $BED_FILE | sort -k1,1 -k2,2n) | bgzip > $SORT_FILE
    tabix -p bed $SORT_FILE
}

do_extract() {
    RESULT_NAME=cis_${BED_FILE}
    tabix $SORT_FILE $REGION > $RESULT_NAME
}

main() {
    prepare_for_tabix
    do_extract
}

main
