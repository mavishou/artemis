#!/usr/bin/env bash

set -o nounset
set -o errexit

_prepare() {
    REGION=chr15:99097229-99725324
    CHIP_DIR=../../../06_ChIP-Seq/call_peak/$ID
}

_bigWig_to_wig() {
    local bw_file=$1
    local wig_file=$2
    bigWigToWig $bw_file $wig_file -chrom=chr15 -start=99097229 -end=99725324
}

convert_ppois() {
    local bw_file=$CHIP_DIR/${ID}_pvalue.bigWig
    local wig_file=hm_${PREFIX}_pvalue.wig
    _bigWig_to_wig $bw_file $wig_file
}

convert_FE() {
    local bw_file=$CHIP_DIR/${ID}_fold_change.bigWig
    local wig_file=hm_${PREFIX}_fold_change.wig
    _bigWig_to_wig $bw_file $wig_file
}

prepare_for_tabix() {
    local bed_file=$CHIP_DIR/macs2_peaks.broadPeak
    SORT_BED=$bed_file.sorted.gz
    (cat $bed_file | sort -k1,1 -k2,2n) | bgzip > $SORT_BED
    tabix -p bed $SORT_BED
}

do_extract_bed() {
    local result_file=hm_${PREFIX}.bed
    tabix $SORT_BED $REGION > $result_file
}


main() {
    while read ID PREFIX; do
        echo $ID $PREFIX
        _prepare
        convert_ppois
        convert_FE
        prepare_for_tabix
        do_extract_bed
    done
}

main


