#!/usr/bin/env bash

BIG_WIG=$1

main() {
    RESULT_FILE=cis_${BIG_WIG%.*}.wig
    bigWigToWig $BIG_WIG $RESULT_FILE -chrom=chr15 -start=99097229 -end=99725324
}

main
