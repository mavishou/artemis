#!/usr/bin/env bash

set -o nounset
# set -o errexit

SUFFIX=$1
BASE_DIR=/lustre/user/houm/projects/Artemis
SRC_BASE=houm@202.205.131.16:$BASE_DIR

TMP_DIR=tmp_rsync
DEPTH=3
LOCAL_PRPROJECT_DIR=/Users/Mavis/Projects/Artemis/

rm -rf $TMP_DIR
mkdir $TMP_DIR
cd $TMP_DIR

n=0
SRC=$SRC_BASE/*

while [[ $n -lt $DEPTH ]]; do
    n=$(($n+1))
    cmd="rsync -arzvPR $SRC.$SUFFIX  ./"
    echo $cmd
    eval $cmd
    SRC=$SRC/*
done

cd $OLDPWD

LOCAL_BASE_DIR=${TMP_DIR}$BASE_DIR
cd $LOCAL_BASE_DIR

n=0
SRC=*
while [[ $n -lt $DEPTH ]]; do
    n=$(($n+1))
    cmd="rsync -arzvPR $SRC.$SUFFIX $LOCAL_PRPROJECT_DIR"
    echo $cmd
    eval $cmd
    SRC=$SRC/*
done

cd $OLDPWD
rm -rf $TMP_DIR
echo "Done!"
