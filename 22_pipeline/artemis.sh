#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

C1=$1
C2=$2
CPU_NUM=2

RNA_SAMPLES=../00_samples/RNA-Seq_final.txt
CHIP_SAMPLES=../00_samples/ChIP-Seq_final.txt
RNA_BASE_DIR=../07_RNA-Seq/fc
CHIP_BASE_DIR=../06_ChIP-Seq/call_peak
exp_rc_file=gene_reads_count
JS_GENE_BED=../01_gene_model/artemis_gene.bed

prepare() {
    DIR=compare/${C1}_vs_${C2}
    mkdir -p $DIR
    exp_sample_sheet=$DIR/exp_samples.txt
    exp_diff_result=$DIR/exp_diff.txt
    all_consensus_bed=$DIR/all_consensus_peaks.bed
    gene_neighbor_peak_file=$DIR/gene_neighbor_peaks.txt
    diff_chip_peak_bed=$DIR/all_diff_peaks.bed
    # k4_peak_diff_result=$DIR/H3K4me3_peak_diff.txt
    # k27_peak_diff_result=$DIR/H3K27ac_peak_diff.txt
}

_prepare_exp_sample_sheet() {
    echo "condition" > $exp_sample_sheet
    cat $RNA_SAMPLES | awk -v C1="$C1" '$1==C1 {print $2"\t"$1}' >> $exp_sample_sheet
    cat $RNA_SAMPLES | awk -v C2="$C2" '$1==C2 {print $2"\t"$1}' >> $exp_sample_sheet
}


_do_exp_diff() {
    cmd="/apps/bioinfo/R-3.2.0/bin/Rscript exp_diff.R $DIR"
    run_cmd "$cmd" "Run diff exp"
}

process_rna_seq() {
    echo "Processing RNA-Seq..."
    _prepare_exp_sample_sheet
    _do_exp_diff
}


_prepare_chip() {
    CHIP_PREFIX=$DIR/${HISTONE}
    chip_sample_sheet=${CHIP_PREFIX}_samples.txt    
    bed_for_merge=${CHIP_PREFIX}_for_merge.bed
    peak_merged_result=${CHIP_PREFIX}_merged.txt
    merged_bed=${CHIP_PREFIX}_merged.bed
    merged_saf=${CHIP_PREFIX}_merged.saf
}

_prepare_chip_sample_sheet() {
    echo "condition" > $chip_sample_sheet
    cat $CHIP_SAMPLES | awk -v C1="$C1" -v his="$HISTONE" '$1==C1 && $2==his {print $3"\t"$1}' >> $chip_sample_sheet
    cat $CHIP_SAMPLES | awk -v C2="$C2" -v his="$HISTONE" '$1==C2 && $2==his {print $3"\t"$1}' >> $chip_sample_sheet
}

__prepare_for_peak_merge() {
    cmd="/apps/bioinfo/R-3.2.0/bin/Rscript prepare_for_peak_merge.R $DIR $HISTONE"
    run_cmd "$cmd" "Prepare for peak merge"
}

__merge_peaks() {
    mergeBed -i $bed_for_merge -c 4 -o collapse > $peak_merged_result
    # convert to bed
    cat $peak_merged_result | awk -v his="$HISTONE" '{print $1"\t"$2"\t"$3"\t"his"_P"NR}' > $merged_bed
    # convert to saf
    cat $peak_merged_result | awk -v his="$HISTONE" '{print his"_P"NR"\t"$1"\t"$2"\t"$3"\t."}' > $merged_saf
}

_get_consensus_peaks() {
    __prepare_for_peak_merge
    __merge_peaks
}

___prepare_fc() {
    chip_bam=/home/gaog_pkuhpc/users/houm/Artemis/06_ChIP-Seq/alignment_hg38/$ID/dedupped.sort.bam
    CHIP_FC_DIR=${CHIP_PREFIX}_fc/$ID
    mkdir -p $CHIP_FC_DIR
    peak_fc_result=$CHIP_FC_DIR/peak_reads_count
}

__run_featureCounts() {
    ___prepare_fc
    CMD_FC="~/users/houm/tools/subread-1.5.1-Linux-x86_64/bin/featureCounts"
    CMD_FC="$CMD_FC -T $CPU_NUM"
    CMD_FC="$CMD_FC -a $merged_saf"
    CMD_FC="$CMD_FC -F SAF"
    CMD_FC="$CMD_FC -o $peak_fc_result"
    CMD_FC="$CMD_FC $chip_bam"    
    run_cmd "$CMD_FC" "Running featureCounts"
}

_count_reads_in_consensus_regions() {
    cat $chip_sample_sheet | sed '1 d' | cut -f 1 | while read ID; do
        __run_featureCounts
    done
}

_do_peak_diff() {
    cmd="/apps/bioinfo/R-3.2.0/bin/Rscript peak_diff.R $DIR $HISTONE"
    run_cmd "$cmd" "Run diff peak"  
}

process_chip_seq() {
    HISTONE=$1
    echo "Processing ChIP-Seq..."
    _prepare_chip
    _prepare_chip_sample_sheet
    _get_consensus_peaks
    _count_reads_in_consensus_regions
    _do_peak_diff
}

calculate_scores() {
    direction=$1
    echo "Calculating scores..."
    cmd="/apps/bioinfo/R-3.2.0/bin/Rscript calculate_score.R $DIR $direction"
    run_cmd "$cmd" "Calculate scores"
}

get_genes_neighbor_peaks() {
    echo "Getting neighbor peaks of genes..."
    cat $DIR/*_merged.bed > $all_consensus_bed
    windowBed -a $JS_GENE_BED -b $all_consensus_bed -w 3000 > $gene_neighbor_peak_file
}

_get_diff_chip_peaks() {
    local diff_peak_name_file=$DIR/all_diff_peak_name.txt
    cat $DIR/H3K27ac_peak_diff.txt $DIR/H3K4me3_peak_diff.txt | awk '$7<=0.05 {print $1}' > $diff_peak_name_file
    ~/bin/myjoin -F 4 $all_consensus_bed $diff_peak_name_file | grep ^= | cut -f 2-5 > $diff_chip_peak_bed
    rm -f $diff_peak_name_file
}

_get_chip_peak_neighbor_hic() {
    local ct=$1
    echo "Getting ChIP-Seq peak and Hi-C peak pairs for $ct"
    local hic_peak_bed=../05_hic_loops/results/${ct}_loop_peaks_hg38.bed
    local chip_peak_hic_pair_file=$DIR/$ct.chip_peak_hic_pairs.txt
    if [[ -s $hic_peak_bed ]]; then
        intersectBed -a $diff_chip_peak_bed -b $hic_peak_bed -wa -wb | cut -f 4,8 > $chip_peak_hic_pair_file
    else
        echo "No Hi-C data for $ct"
    fi
}

process_hic() {
    echo "Processing Hi-C data" 
    _get_diff_chip_peaks
    _get_chip_peak_neighbor_hic $C1
    _get_chip_peak_neighbor_hic $C2
}

main() {
    echo $C1 $C2
    prepare
    #process_rna_seq
    #process_chip_seq H3K4me3
    #process_chip_seq H3K27ac
    #get_genes_neighbor_peaks
    process_hic
    calculate_scores up
    calculate_scores down
    echo "Done!"
}

main

