#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2017/3/21


from myToolKit.general_helper import *


def process_src(full_src):
    l1 = full_src.split('_vs_')
    c1 = l1[0]
    tmp = l1[1]
    if tmp.endswith('up'):
        c2 = tmp.replace('_up', '')
        up_cell = c2
    elif tmp.endswith('down'):
        up_cell = c1
    return up_cell

for line in sys.stdin.readlines():
    srcs = line.rstrip('\n').split(', ')
    cells = set()
    for src in srcs:
        cell = process_src(src)
        cells.add(cell)
    print ', '.join(cells)
