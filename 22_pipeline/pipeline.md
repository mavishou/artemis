Pipeline to find cis regulatory lncRNAs

# Datasets
See `../00_smaples/samples.xlsx`

* Cell line: description of cell lines. Finally remained 6 cell lines.
* RNA-Seq: selected RNA-Seq datasets
* RNA-Seq check: RNA-Seq samples with titles for check
* RNA-Seq to run 0107: output of `clean_RNA_Seq_samples.R`
* RNA-Seq grouped: the required sample list to perform analysis
* ChIP-Seq: selected ChIP-Seq datasetes
* status: to record the status to process the datasets

# Batch process
## Download
### Convert ID
```bash
# On Solar
cat gsm_batch_1 | python ID_convert.py -m full -p batch_1
# Generated files: batch_1_url for download and batch_1_ID_map
```
### Do the download
It's ultrafast to download by ascp

```bash
# On Solar
cat batch_1_url | bash ascp_download.sh batch_1
```
Then sra files are downloaded in `../00_samples/sra/batch_1`

### Copy to the HPC
```bash
# On HPC, login to login10 node
cd /home/gaog_pkuhpc/users/houm/Artemis/00_samples/sra
rsync -arzvP houm@202.205.131.15:/lustre/user/houm/projects/Artemis/00_samples/sra/batch_1 ./
```

## Convert sra to fastq
```bash
# On HPC
cd ../00_samples
cat RNA-Seq_grouped_0117.txt | hpc_batch -s do_fastq_dump.sh -x HM-DB1 -c 1
ls sra/batch_1/ | sed 's:^:sra/batch_1/:' | hpc_batch -s do_fastq_dump.sh -x HM-DB1 -c 1
```

## RNA-Seq analysis
### Alignment
```bash
# On HPC
cd ../07_RNA-Seq
cat ../00_samples/RNA-Seq_grouped_0117.txt | hpc_batch -s do_alignment_hg38.sh -x HM-HS -c 5
# Result directory: ../07_RNA-Seq/alignment_hg38
```
#### Stat 
```bash
ls logs | grep HS | while read f; do perl /lustre1/gaog_pkuhpc/users/tianf/Finished/03-LocExpress/10_alignment/do_bam_stat.pl logs/$f >> tmp.txt; done
cat tmp.txt | awk '$2 > 0' | sed -r 's/^.+(GSM[0-9_]+)-.+log/\1/' > tmp2.txt
myjoin -F 2 ../00_samples/RNA-Seq_final.txt tmp2.txt | grep ^= | cut -f 2,4-10 > stat_reads_for_paper.txt
rm -f tmp*.txt
```

### Count reads
```bash
# cat ../00_samples/RNA-Seq_grouped_0117.txt | cut -f 1 | hpc_batch -s do_featureCounts.sh -x HM-FA -c 5
cat ../00_samples/RNA-Seq_final.txt | cut -f 2 | hpc_batch -s do_featureCounts.sh -c 10 -x HM-FC
```

### Call expression
#### Run
```bash
cat ../00_samples/RNA-Seq_final.txt | cut -f 2 | hpc_batch -s call_exp.sh -c 10 -x HM-ST
```
#### Merge result
* Merge TPMs by `merge_exp.R`. 
* Results
	* `sample_tpm.txt`: TPMs of each sample
	* `cell_type_tpm_mean.txt`: the most used one
	* `cell_type_tpm_median.txt`: the median TPM for each cell type.

#### The distribution of TPMs

<img src="media/14890365247147.jpg" width=600>

### Stat
```bash
# On HPC
# stat from the log file of featureCounts
hpc_run 'grep "Total fragments" logs/* > stat_out.txt'
cat stat_out.txt | sed -r 's/.+(GSM[0-9]+)-.+:||/\1/' | awk '{print $1"\t"$2}' | sort | uniq > stat_matched_reads.txt
rm -f stat_out.txt
```


## ChIP-Seq analysis
### Alignment
#### Build index for bowtie2
```bash
# On HPC, only contain main chromosomes
cd /home/gaog_pkuhpc/users/houm/genome/human/hg38/bowtie2
hpc_run -J "HM-I" -c 20 "~/users/houm/tools/bowtie2-2.3.0-legacy/bowtie2-build --threads 20 ../sequences/chr10.fa,../sequences/chr11.fa,../sequences/chr12.fa,../sequences/chr13.fa,../sequences/chr14.fa,../sequences/chr15.fa,../sequences/chr16.fa,../sequences/chr17.fa,../sequences/chr18.fa,../sequences/chr19.fa,../sequences/chr1.fa,../sequences/chr20.fa,../sequences/chr21.fa,../sequences/chr22.fa,../sequences/chr2.fa,../sequences/chr3.fa,../sequences/chr4.fa,../sequences/chr5.fa,../sequences/chr6.fa,../sequences/chr7.fa,../sequences/chr8.fa,../sequences/chr9.fa,../sequences/chrM.fa,../sequences/chrX.fa,../sequences/chrY.fa genome”
```
#### Do alignment
```bash
# On HPC
cd ../06_ChIP-Seq
cat ../00_samples/ChIP-Seq_for_mapping.txt | hpc_batch -s do_alignment_hg38.sh -x HM-BT -c 5
```
### Call peak
```
cat ../00_samples/ChIP-Seq_call_peak.txt | hpc_batch -s call_peak.sh -x HM-CP -c 1
```

### Filter peaks
```bash
cat ../00_samples/ChIP-Seq_replicates.txt | hpc_batch -s filter_peaks.sh -x HM-F -c 1
```
### Build index for overlapped peaks
```bash
cat ../00_samples/ChIP-Seq_final.txt | cut -f 3 | hpc_batch -s build_index_for_peaks.sh -x HM-IX
```
### Stat 
```bash
ls logs | grep B | while read f; do perl /lustre1/gaog_pkuhpc/users/tianf/Finished/03-LocExpress/10_alignment/do_bam_stat.pl logs/$f >> tmp.txt; done
#cat tmp.txt | awk '$2 > 0' | sed -r 's/^.+(GSM[0-9_]+)-.+log/\1/' > tmp2.txt
cat tmp.txt | awk '$2 > 0 && $1!~/err/' | sed -r 's/^.+(GSM[0-9_]+)-.+log/\1/' > tmp2.txt
myjoin -F 3 ../00_samples/ChIP-Seq_final.txt tmp2.txt | grep ^= | cut -f 2,4-10 | uniq > stat_bowtie_reads_for_paper.txt

grep bam_rmdupse_core logs/* | sed -r 's/^.+(GSM[0-9_]+)-.+log:\[bam_rmdupse_core\] ([0-9]+) \/ ([0-9]+) = .+/\1\t\2\t\3/' > tmp.txt
myjoin -F 3 ../00_samples/ChIP-Seq_final.txt tmp.txt | grep ^= | cut -f 2,3,4,6,7 > stat_dedup_reads.txt
rm -f tmp*.txt
```

## Hi-C
### Get Hi-C loop peaks that match lnc-PCG pairs
```bash
# on solar
cd 05_hic_loops
cat ../00_samples/HiC_cell_lines.txt | bash process_hic.sh

# stat
wc -l cell_lines/*/lnc_PCG_HiC.txt
   7403 cell_lines/GM12878/lnc_PCG_HiC.txt
   4244 cell_lines/HeLa-S3/lnc_PCG_HiC.txt
   5784 cell_lines/HUVEC/lnc_PCG_HiC.txt
   6849 cell_lines/IMR90/lnc_PCG_HiC.txt
   6818 cell_lines/K562/lnc_PCG_HiC.txt
   6134 cell_lines/NHEK/lnc_PCG_HiC.txt

for f in $(ls cell_lines/*/lnc_PCG_HiC.txt); do cat $f | cut -f 1,3 | uniq | wc -l; done
7000
4164
5638
6518
6647
5896  
```

# Integrate
## Datasets

### Get all condition pairs
```bash
cd 00_samples
cat RNA-Seq_final.txt | cut -f 1 | uniq > all_cell_lines.txt
cat all_cell_lines.txt | while read c1; do cat all_cell_lines.txt | while read c2; do  if [[ $c1 < $c2 ]]; then echo -e "$c1\t$c2";  elif  [[ $c1 > $c2 ]]; then echo -e "$c2\t$c1";  fi; done; done | sort | uniq > all_compares.txt
```

## Run all conditions
```bash
# run
cat ../00_samples/all_compares.txt | hpc_batch -s artemis.sh -x HM-AR -c 2

# check result
for d in $(ls compare); do ls -lh compare/$d/up_final_scores.txt; done
for d in $(ls compare); do ls -lh compare/$d/down_final_scores.txt; done

# get the result back to solar
rsync -arzvPR compare/*/up_final_scores.txt houm@202.205.131.15:/lustre/user/houm/projects/Artemis/22_pipeline/
rsync -arzvPR compare/*/down_final_scores.txt houm@202.205.131.15:/lustre/user/houm/projects/Artemis/22_pipeline/
```
## Tidy compare result
Use `tidy_compare_result.R`

