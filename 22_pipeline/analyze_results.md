# Get high-confident pairs
## High-confident pairs for the four score
```bash
cat all_sig_compare_result.txt | head -1 > 4_scores_high_conf.txt
# 28: final_sig; 30: final_padj
cat all_sig_compare_result.txt | awk '$28==1 && $30 <= 0.05' >> 4_scores_high_conf.txt

# How many lnc-PCG pairs?
cat 4_scores_high_conf.txt | cut -f 3,4 | sort | uniq | wc -l
15383
# How many lncRNAs?
cat 4_scores_high_conf.txt | cut -f 3 | sed '1d' | sort | uniq | wc -l
4889
```

## High confident pairs considering Hi-C
```bash
# $5==2: two Hi-C peaks are supported by ChIP-Seq data
# $6==1: all four score are significant
# $7<=100: rank is less than 100
cat all_sig_compare_result.txt | cut -f 1,2,3,4,22,28,31 | awk '$5==2 && $6==1 && $7<=100' | sort -nk7,7 > high_conf.txt
cat high_conf.txt | cut -f 3,4 | sort -u > high_conf_pairs.txt

# How many pairs?
wc -l high_conf_pairs.txt
735
# How many lncRNAs
cat high_conf_pairs.txt | cut -f 1 | sort -u | wc -l
605
```
Then use `get_exp_for_high_conf.R` to add expression for `high_conf.txt`, the result is `high_conf_with_exp.txt`

```bash
cd final
cat to_report.txt | sed '1 d' | awk '$7==1' | cut -f 3,4 | awk '{print $1"\n"$2}' | sort -u > ID_to_convert.txt
cat ID_to_convert.txt | grep ^ENSG | sed -r 's/\.[0-9]+//'> ENSG_to_convert.txt
cat ID_to_convert.txt | grep R | sed -r 's/\.[0-9]+//'> RefSeq_to_convert.txt

# filter lnc
cat final/to_report.txt | head -1 > final/to_report_filter_lnc.txt
myjoin -F 3 -f 4 final/to_report.txt ../01_gene_model/lnc_to_report.bed | grep ^= | cut -f 2-9 >> final/to_report_filter_lnc.txt
```


