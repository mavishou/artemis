# Get pairs of lncRNA genes and PCGs
[TOC]

**Requirement:** 

* One is pure lncRNA gene and the other is protein coding gene
* The interval should be **less than 1Mb** and **larger than 10Kb**

## Get pure lncRNA genes
* Script: `../01_gene_model/pure_lnc.R`
* Result: `../01_gene_model/artemis_pure_lnc_genes.txt`

## Get protein coding genes
```bash
cd ../01_gene_model
cat JS_exon_full.gtf | grep "protein_coding" | awk '{print $10}' | sed 's/;//' | sed 's/"//g' | uniq | sort | uniq > PCG.txt
```

## Get the bed of lncRNA genes and PCGs
* Script: `prepare_bed.R`
* Results:
	* bed of lncRNA genes: `../01_gene_model/artemis_gene_lnc.bed`
	* bed of PCGs: `../01_gene_model/JS_gene_PCG.bed`
	
## Get pairs
### windowBed
```bash
windowBed -a ../01_gene_model/artemis_gene_lnc.bed -b ../01_gene_model/JS_gene_PCG.bed -w 1000000 > lnc_PCG_neighbor.txt
```

```bash
# Here are some backups. They are not used finally.

## Get lncRNA loci
# cat JS_gene_lnc.bed | sort -k1,1 -k2,2n > JS_gene_lnc_sorted.bed
# mergeBed -c 4 -o collapse -i JS_gene_lnc_sorted.bed > JS_merged_lnc_gene.txt

## Get loci
# cat JS_gene.bed | sort -k1,1 -k2,2n > JS_gene_sorted.bed
# mergeBed -c 4 -o collapse -i JS_gene_sorted.bed > JS_merged_gene.txt

## For getting intergenic
# intersectBed -a JS_gene_lnc.bed -b JS_gene.bed -c > JS_lnc_overlap_count.txt
```

### Clean the output of windowBed
See `pair_lnc_PCG.R`

Results:
> Note: the first is lncRNA gene and the second is PCG

* `lnc_PCG_full.txt`: full results of pairs
* `lnc_PCG.txt`: only contain names, interval and position
* `lnc_PCG_filtered.txt`: filtered by interval >= 3Kb

> For position:
> 
> * +: lncRNA is in front of other
> * -: PCG is in front of lncRNA
> * =: the start positions are the same


