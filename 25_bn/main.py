#! /usr/bin/env python
# by caozj
# 2017-02-26 20:29

"""
This script is the actual testing script that invokes BN for scoring
"""

import sys
from multiprocessing import Pool
from subprocess import call

import numpy as np
import pandas as pd

from cis_bn import CisBN


#===============================================================================
#
#  TODO: Tweak parameters here
#
#===============================================================================

njobs = 10  # How many parallel process to use (with multiprocessing module)
adjust_pvalue = True

# adjust weight
if adjust_pvalue:
    # If p-value is adjusted, weight can be more evenly distributed
    weight_dict = {
        'CI': 5,
        'PP': 5,
        'LP': 5,
        'PE': 5,
        'LE': 5,
        'nH': 5,
        'nHP': 2,
        'nHC': 1.5
    }
else:
    weight_dict = {
        'CI': 5,
        'PP': 2,
        'LP': 2,
        'PE': 10,
        'LE': 15,
        'nH': 5,
        'nHP': 2,
        'nHC': 2
    }


#===============================================================================
#
#  Command line arguments
#
#===============================================================================

# Assign command line arguments
comparison = sys.argv[1]  # e.g. 'GM12878_vs_T-ALL'
direction = sys.argv[2]  # Either 'up' or 'down'
mode = sys.argv[3]  # One of 'all', 'first' or 'random'
if mode != 'all':
    sample_count = int(sys.argv[4])  # How many pairs to subset (either first, or random)
    # Give the case to look for (the script will ensure that the case is in the subset)
    case_lncRNA = sys.argv[5]
    case_PCG = sys.argv[6]


#===============================================================================
#
#  Scoring is done below
#
#===============================================================================

# Read data
obs_df = pd.read_table('../raw/cmp/%s/%s_final_scores.txt' % (comparison, direction))
obs_df = obs_df.ix[:, ['lncRNA', 'PCG',  # Pair identifier
                       'lnc_exp_sig', 'PCG_exp_sig', 'lnc_peak_sig', 'PCG_peak_sig',  # only for plotting
                       'lnc_exp_pval', 'PCG_exp_pval', 'lnc_peak_pval', 'PCG_peak_pval',
                       'hic_match', 'num_hic_chip_support', 'num_hic_has_motif',
                       'final_score', 'final_padj', 'rank']]  # original performance


# Rename columns
tmp_cols = []
for cname in obs_df.columns:
    if cname == 'lnc_exp_pval':
        tmp_cols.append('LE')
    elif cname == 'PCG_exp_pval':
        tmp_cols.append('PE')
    elif cname == 'lnc_peak_pval':
        tmp_cols.append('LP')
    elif cname == 'PCG_peak_pval':
        tmp_cols.append('PP')
    elif cname == 'hic_match':
        tmp_cols.append('nH')
    elif cname == 'num_hic_chip_support':
        tmp_cols.append('nHP')
    elif cname == 'num_hic_has_motif':
        tmp_cols.append('nHC')
    else:
        tmp_cols.append(cname)
obs_df.columns = tmp_cols

HiC_available = np.any(obs_df['nH'] != 0)
print('HiC availability: ' + str(HiC_available))


# TODO: method of adjusting p_value
if adjust_pvalue:
    print('\nAdjusting p-value...')
    print('PE normalization factor: %f' % abs(np.percentile(np.log10(obs_df['PE']), 3)))
    print('LE normalization factor: %f' % abs(np.percentile(np.log10(obs_df['LE']), 3)))
    print('PP normalization factor: %f' % abs(np.percentile(np.log10(obs_df['PP']), 3)))
    print('LP normalization factor: %f' % abs(np.percentile(np.log10(obs_df['LP']), 3)))
    obs_df['PE'] = 10 ** ((np.log10(obs_df['PE']) * 10 / abs(np.percentile(np.log10(obs_df['PE']), 3))))
    obs_df['LE'] = 10 ** ((np.log10(obs_df['LE']) * 10 / abs(np.percentile(np.log10(obs_df['LE']), 3))))
    obs_df['PP'] = 10 ** ((np.log10(obs_df['PP']) * 10 / abs(np.percentile(np.log10(obs_df['PP']), 3))))
    obs_df['LP'] = 10 ** ((np.log10(obs_df['LP']) * 10 / abs(np.percentile(np.log10(obs_df['LP']), 3))))
    print()
    # Bug spotted: minimal pval can be 0, changed to percentile


# Subsetting
np.random.seed(0)
if mode == 'random':
    obs_df_sample_idx = np.random.choice(obs_df.index, sample_count, replace=False)
elif mode == 'first':
    obs_df_sample_idx = [i for i in range(0, sample_count)]
elif mode == 'all':
    obs_df_sample_idx = [item for item in obs_df.index]
    sample_count = obs_df.shape[0]
else:
    raise Exception('Invalid mode!')

if mode != 'all':
    case_idx = np.where((obs_df.ix[:, 'lncRNA'] == case_lncRNA) & (obs_df.ix[:, 'PCG'] == case_PCG))[0].item(0)
    if case_idx not in obs_df_sample_idx:
        obs_df_sample_idx[0] = case_idx  # Make sure that the case is in the subset


# Splitting data for parallel computing
parallel_sample_list = []
block_size = sample_count // njobs
for i in range(0, njobs):
    parallel_sample_list.append(obs_df_sample_idx[ (i * block_size) : ((i + 1) * block_size) ])
parallel_sample_list[-1] = np.append(parallel_sample_list[-1], obs_df_sample_idx[(njobs * block_size):])
# Append remaining samples to the last block


# Instantiate network and set parameters
cis_prior = 0.05
network = CisBN(cis_prior = 0.05)
for weight_name in weight_dict.keys():
    network.assign_cpt_by_weight(weight_name, weight_dict[weight_name])


# Report network parameters for logging
print('cis_prior = %f' % cis_prior)
print()
for cpt_name in ('CI', 'PP', 'LP', 'PE', 'LE', 'nH', 'nHP', 'nHC'):
    print('----CPT of %s----' % cpt_name)
    print(network.cpt_dict[cpt_name])
    print()


def parallel_eval(sample_idx):
    """
    Function to be called in parallel
    """
    result = []
    for row in sample_idx:
        pp_pval = obs_df.ix[row, 'PP']
        lp_pval = obs_df.ix[row, 'LP']
        pe_pval = obs_df.ix[row, 'PE']
        le_pval = obs_df.ix[row, 'LE']
        if HiC_available:
            network.assign_n('nH', obs_df.ix[row, 'nH'])
            network.assign_n('nHP', obs_df.ix[row, 'nHP'])
            network.assign_n('nHC', obs_df.ix[row, 'nHC'])
        # Generate a marker for taking the actual limit inside evaluation()
        network.assign_cred('PP', (pp_pval / (1 - pp_pval)) if pp_pval != 1 else np.inf)
        network.assign_cred('LP', (lp_pval / (1 - pp_pval)) if pp_pval != 1 else np.inf)
        network.assign_cred('PE', (pe_pval / (1 - pp_pval)) if pp_pval != 1 else np.inf)
        network.assign_cred('LE', (le_pval / (1 - pp_pval)) if pp_pval != 1 else np.inf)
        cis_prob = network.evaluate(HiC_available)
        result.append(cis_prob)
    return result


# # Single process debugging
# rs = parallel_eval(parallel_sample_list[0])
# import os; os._exit(0)


# Parallel computing
print('Computing score in parallel...')
pool = Pool(njobs)
pool_rs = pool.map(parallel_eval, parallel_sample_list)
combined_rs = []
for i in range(0, njobs):
    combined_rs += pool_rs[i]  # Merge result
obs_df = obs_df.ix[obs_df_sample_idx, :]
obs_df['cis_prob'] = combined_rs
pool.close()


# Save result
print('Saving result...')
if mode == 'all':
    obs_df.ix[obs_df_sample_idx, :].to_csv('../result/bn_%s_%s_%s.csv' % (comparison, direction, mode))
else:
    obs_df.ix[obs_df_sample_idx, :].to_csv('../result/bn_%s_%s_%s_%d.csv' % (comparison, direction, mode, sample_count))


print('Calling R to make plots...')
if mode == 'all':
    call(["./bn_result_inspect.R", comparison, direction, mode])
else:
    call(["./bn_result_inspect.R", comparison, direction, mode, str(sample_count)])

