#! /bin/bash
# by caozj
# 2017-03-15 15:59


# Run main.py batch-wise


# Compute score

# ## LUNAR1
# python -u main.py GM12878_vs_T-ALL   up   all | tee ../log/GM12878_vs_T-ALL_up.log
# python -u main.py K562_vs_T-ALL      up   all | tee ../log/K562_vs_T-ALL_up.log
python -u main.py T-ALL_vs_T_Cell    down all | tee ../log/T-ALL_vs_T_Cell_down.log
# 
# ## CCAT1-L
# python -u main.py BJ_vs_HeLa-S3      up   all | tee ../log/BJ_vs_HeLa-S3.log
# python -u main.py GM12878_vs_HeLa-S3 up   all | tee ../log/GM12878_vs_HeLa-S3.log
# python -u main.py HeLa-S3_vs_T-ALL   down all | tee ../log/HeLa-S3_vs_T-ALL.log
# python -u main.py HeLa-S3_vs_T_Cell  down all | tee ../log/HeLa-S3_vs_T_Cell.log
# 
# ## HOTTIP
# python -u main.py BJ_vs_H1           down all | tee ../log/BJ_vs_H1.log
# python -u main.py BJ_vs_IMR90        down all | tee ../log/BJ_vs_IMR90.log
# python -u main.py BJ_vs_K562         down all | tee ../log/BJ_vs_K562.log
# 
# ## ncRNA-a3
# python -u main.py IMR90_vs_K562      up   all | tee ../log/IMR90_vs_K562.log
# python -u main.py K562_vs_T_Cell     down all | tee ../log/K562_vs_T_Cell.log


# Make case plots

# ## LUNAR1
# ./case_plot.R GM12878_vs_T-ALL   up   NR_126487.1       ENSG00000140443.13 all
# ./case_plot.R K562_vs_T-ALL      up   NR_126487.1       ENSG00000140443.13 all
./case_plot.R T-ALL_vs_T_Cell    down NR_126487.1       ENSG00000140443.13 all
# 
# ## CCAT1-L
# ./case_plot.R BJ_vs_HeLa-S3      up   ENSG00000247844.1 ENSG00000136997.14 all
# ./case_plot.R GM12878_vs_HeLa-S3 up   ENSG00000247844.1 ENSG00000136997.14 all
# ./case_plot.R HeLa-S3_vs_T-ALL   down ENSG00000247844.1 ENSG00000136997.14 all
# ./case_plot.R HeLa-S3_vs_T_Cell  down ENSG00000247844.1 ENSG00000136997.14 all
# 
# ## HOTTIP
# ./case_plot.R BJ_vs_H1           down ENSG00000243766.7 ENSG00000253293.4  all
# ./case_plot.R BJ_vs_IMR90        down ENSG00000243766.7 ENSG00000253293.4  all
# ./case_plot.R BJ_vs_IMR90        down ENSG00000243766.7 ENSG00000005073.5  all
# ./case_plot.R BJ_vs_IMR90        down ENSG00000243766.7 ENSG00000078399.15 all
# ./case_plot.R BJ_vs_K562         down ENSG00000243766.7 ENSG00000005073.5  all
# ./case_plot.R BJ_vs_K562         down ENSG00000243766.7 ENSG00000253293.4  all
# ./case_plot.R BJ_vs_K562         down ENSG00000243766.7 ENSG00000078399.15 all
# 
# ## ncRNA-a3
# ./case_plot.R IMR90_vs_K562      up   ENSG00000225506.2 ENSG00000162367.11 all
# ./case_plot.R K562_vs_T_Cell     down ENSG00000225506.2 ENSG00000162367.11 all


echo 'Done!'

