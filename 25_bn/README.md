# README

## cis_bn.py

Implements the specific Bayesian network for scoring lncRNA-PCG
pairs for cis regulatory function

## main.py

Read data, adjust p-value and compute score
(dependent on `cis_bn.py`)

## bn_result_inspect.R

Make some plots to compare bn given score and original score
(Called by main.py)

## batch.sh

Run main.py in batch

## case_plot.R

Highlight the case
(Called by batch.sh)

