#! /usr/bin/env python
# by caozj
# 2017-02-26 15:42

"""
This file defines a lncRNA cis function Bayesian network
"""

import numpy as np
import pandas as pd

class CisBN:

    """
    This is a Bayesian network aimed at scoring lncRNA for cis functions.
    It is not dependent on general BN framework, but an ab initio implementation
    of the specific model.
    """

    def __init__(self, cis_prior=0.5):

        """
        Constructor, also assigning CPTs by default values
        """

        # The following values are parameter of the network
        self.cpt_dict = dict()
        self.cis_prior = cis_prior

        ## Assumption nodes
        self.cpt_dict['CI'] = pd.DataFrame(np.array([
            [0, 0, 0.95],
            [0, 1, 0.05],
            [1, 0, 0.05],
            [1, 1, 0.95]
        ]), columns=['cis', 'CI', 'prob'])
        self.cpt_dict['PP'] = pd.DataFrame(np.array([
            [0, 0, 0.95],
            [0, 1, 0.05],
            [1, 0, 0.05],
            [1, 1, 0.95]
        ]), columns=['cis', 'PP', 'prob'])
        self.cpt_dict['LP'] = pd.DataFrame(np.array([
            [0, 0, 0.95],
            [0, 1, 0.05],
            [1, 0, 0.05],
            [1, 1, 0.95]
        ]), columns=['cis', 'LP', 'prob'])
        self.cpt_dict['PE'] = pd.DataFrame(np.array([
            [0, 0, 0.95],
            [0, 1, 0.05],
            [1, 0, 0.05],
            [1, 1, 0.95]
        ]), columns=['cis', 'PE', 'prob'])
        self.cpt_dict['LE'] = pd.DataFrame(np.array([
            [0, 0, 0.95],
            [0, 1, 0.05],
            [1, 0, 0.05],
            [1, 1, 0.95]
        ]), columns=['cis', 'LE', 'prob'])

        ## Observation of CI node
        self.cpt_dict['nH'] = pd.DataFrame(np.array([
            [0, 0, 0.95],
            [0, 1, 0.05],
            [1, 0, 0.05],
            [1, 1, 0.95],
        ]), columns=['CI', 'nH', 'prob'])
        self.cpt_dict['nHP'] = pd.DataFrame(np.array([
            [0, 0, 0.90],
            [0, 1, 0.05],
            [0, 2, 0.05],
            [1, 0, 0.10],
            [1, 1, 0.30],
            [1, 2, 0.60]
        ]), columns=['CI', 'nHP', 'prob'])
        self.cpt_dict['nHC'] = pd.DataFrame(np.array([
            [0, 0, 0.90],
            [0, 1, 0.05],
            [0, 2, 0.05],
            [1, 0, 0.25],
            [1, 1, 0.35],
            [1, 2, 0.40]
        ]), columns=['CI', 'nHC', 'prob'])

        ## Observation of PP, LP, PE and LE are represented by
        ## p-value derived credibility instead of CPTs

        # The following values represent state of the network (observation)
        self.cred_dict = {
            'PP': None,
            'LP': None,
            'PE': None,
            'LE': None,
            'CI': None
        }
        self.nH = None
        self.nHP = None
        self.nHC = None


    def assign_cpt_direct(self, name, cpt):
        self.cpt_dict[name] = cpt


    def assign_cpt_by_weight(self, name, beta):

        """
        This function uses weights to set CPTs

        As for evidence = 'PP', 'LP', 'PE', 'LE', 'CI',
        beta is defined as below:
            beta  = P(evi = 0 | cis = 0) / P(evi = 0 | cis = 1)
        The symmetrical value alpha is defined as:
            alpha = P(evi = 1 | cis = 0) / P(evi = 1 | cis = 1)

        As for evidence = 'nH':
        beta and alpha are defined similarly:
            beta  = P(nH = 0 | CI = 0) / P(nH = 0 | CI = 1)
            alpha = P(nH = 1 | CI = 0) / P(nH = 1 | CI = 1)

        As for evidence = 'nHP' or 'nHC':
            we define probabilities:
                w0 = prob of not observing HP (or HC) for either lncRNA or PCG, given CI = 0
                w1 = prob of not observing HP (or HC) for either lncRNA or PCG, given CI = 1
            beta is the odds ratio w0 / w1
            alpha is the odds ratio (1 - w0) / (1 - w1)
            Prob of 'nHP' and 'nHC' given cis = 0 would look like this:
                P(nHP = 0 | cis = 0) = w0 ^ 2
                P(nHP = 1 | cis = 0) = 2 * w0 * (1 - w0)
                P(nHP = 2 | cis = 0) = (1 - w0) ^ 2
            cis = 1 follows similar rules, only substituting w1 for w0

        Values in CPT are solved with alpha and beta

        In order to shrink parameter space, it is assumed
        that the symmetrical value alpha = 1 / beta
        """

        if name in ('PP', 'LP', 'PE', 'LE', 'CI'):
            cpt_col = ['cis', name, 'prob']
        elif name in ('nH', 'nHP', 'nHC'):
            cpt_col = ['CI', name, 'prob']
        else:
            raise KeyError('[assign_cpt_by_weight] Invalid name!')

        alpha = 1 / beta

        if name in ('PP', 'LP', 'PE', 'LE', 'CI', 'nH'):
            new_cpt = pd.DataFrame(np.array([
                [0, 0, (beta - beta * alpha) / (beta - alpha)],
                [0, 1, (beta * alpha - alpha) / (beta - alpha)],
                [1, 0, (1 - alpha) / (beta - alpha)],
                [1, 1, (beta - 1) / (beta - alpha)]
            ]), columns=cpt_col)
        elif name in ('nHP', 'nHC'):
            w0 = (alpha * beta - beta) / (alpha - beta)
            w1 = (alpha - 1) / (alpha - beta)
            new_cpt = pd.DataFrame(np.array([
                [0, 0, w0 ** 2],
                [0, 1, 2 * w0 * (1 - w0)],
                [0, 2, (1 - w0) ** 2],
                [1, 0, w1 ** 2],
                [1, 1, 2 * w1 * (1 - w1)],
                [1, 2, (1 - w1) ** 2]
            ]), columns=cpt_col)

        if np.any(new_cpt['prob'] < 0):
            print(new_cpt)
            raise ValueError('[assign_cpt_by_weight] Invalid weights!')
        self.cpt_dict[name] = new_cpt


    def get_cpt(self, name, val_list):

        """
        Fetches values from CPT
        """

        if name in self.cred_dict.keys() or name == 'nH':
            # All values are binary, uses bin2dec encoding for acceleration
            bin_str = ''.join([str(item) for item in val_list])
            idx = int(bin_str, 2)
            return self.cpt_dict[name].ix[idx, -1]
        elif name in ('nHP', 'nHC'):
            # Cannot use the above acceleration
            mask = np.ones(self.cpt_dict[name].shape[0]).astype(bool)
            for val_idx in range(0, len(val_list)):
                mask &= self.cpt_dict[name].ix[:, val_idx] == val_list[val_idx]
            return self.cpt_dict[name].ix[mask, -1].values.item(0)
        else:
            raise KeyError('[get_cpt] Invalid name!')


    def assign_cred(self, name, cred_val):

        """
        Assigns credibility for assumption nodes
        """

        if name not in self.cred_dict.keys() or name == 'CI':
            raise KeyError('[assign_cred] Invalid name!')
        self.cred_dict[name] = cred_val


    def assign_n(self, name, value):

        """
        Assigns n values for CI child nodes
        """

        if name == 'nH':
            self.nH = value
        elif name == 'nHP':
            self.nHP = value
        elif name == 'nHC':
            self.nHC = value
        else:
            raise KeyError('[assign_n] Invalid name!')


    def evaluate(self, useCI = True):

        """
        Evaluates posterior probability of the 'cis' node
        """

        cis_ratio = (1 - self.cis_prior) / self.cis_prior

        # Compute CI credibility
        self.cred_dict['CI'] = 1
        if useCI:
            self.cred_dict['CI'] *= self.get_cpt('nH',  [0, self.nH ])  / self.get_cpt('nH', [1, self.nH ])
            self.cred_dict['CI'] *= self.get_cpt('nHP', [0, self.nHP]) / self.get_cpt('nHP', [1, self.nHP])
            self.cred_dict['CI'] *= self.get_cpt('nHC', [0, self.nHC]) / self.get_cpt('nHC', [1, self.nHC])

        # Compute individual evidence
        if self.cred_dict['CI'] == np.inf:
            ci_ratio = self.get_cpt('CI', [0, 0]) / self.get_cpt('CI', [1, 0])
        else:
            ci_ratio =  (1 + (self.cred_dict['CI'] - 1) * self.get_cpt('CI', [0, 0]))
            ci_ratio /= (1 + (self.cred_dict['CI'] - 1) * self.get_cpt('CI', [1, 0]))
        if self.cred_dict['PP'] == np.inf:
            pp_ratio = self.get_cpt('PP', [0, 0]) / self.get_cpt('PP', [1, 0])
        else:
            pp_ratio =  (1 + (self.cred_dict['PP'] - 1) * self.get_cpt('PP', [0, 0]))
            pp_ratio /= (1 + (self.cred_dict['PP'] - 1) * self.get_cpt('PP', [1, 0]))
        if self.cred_dict['LP'] == np.inf:
            lp_ratio = self.get_cpt('LP', [0, 0]) / self.get_cpt('LP', [1, 0])
        else:
            lp_ratio =  (1 + (self.cred_dict['LP'] - 1) * self.get_cpt('LP', [0, 0]))
            lp_ratio /= (1 + (self.cred_dict['LP'] - 1) * self.get_cpt('LP', [1, 0]))
        if self.cred_dict['PE'] == np.inf:
            pe_ratio = self.get_cpt('PE', [0, 0]) / self.get_cpt('PE', [1, 0])
        else:
            pe_ratio =  (1 + (self.cred_dict['PE'] - 1) * self.get_cpt('PE', [0, 0]))
            pe_ratio /= (1 + (self.cred_dict['PE'] - 1) * self.get_cpt('PE', [1, 0]))
        if self.cred_dict['LE'] == np.inf:
            le_ratio = self.get_cpt('LE', [0, 0]) / self.get_cpt('LE', [1, 0])
        else:
            le_ratio =  (1 + (self.cred_dict['LE'] - 1) * self.get_cpt('LE', [0, 0]))
            le_ratio /= (1 + (self.cred_dict['LE'] - 1) * self.get_cpt('LE', [1, 0]))

        # Compute posterior probability
        total_ratio = cis_ratio * ci_ratio * pp_ratio * lp_ratio * pe_ratio * le_ratio
        return 1 / (1 + total_ratio)

