#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2017/3/8

from myToolKit.general_helper import *


def make_loc_name(number):
    add_zeros = '00000' + str(number)
    l = len(add_zeros)
    correct_len = add_zeros[l-6:]
    return 'LOC' + correct_len


def main(merge_file, locus_file, map_file):
    with open(merge_file) as reader, open(locus_file, 'w') as locus_writer, open(map_file, 'w') as map_writer:
        loc_num = 0
        for line in reader:
            loc_num += 1
            cols = line_to_list(line)
            trans_ids = cols[3].split(',')

            locus_name = make_loc_name(loc_num)
            cols[3] = locus_name

            locus_writer.write(get_output_line(cols))

            for trans_id in trans_ids:
                map_writer.write(get_output_line([trans_id, locus_name]))


if __name__ == '__main__':
    # input_file = 'JS_lncRNA_merged.bed'
    input_file = 'MSTRG_multi_exon_lincRNA_merged.bed'
    # output_file_1 = 'lncRNA_locus.bed'
    output_file_1 = 'MSTRG_multi_exon_lincRNA_locus.bed'
    # output_file_2 = 'lncRNA_locus_map.txt'
    output_file_2 = 'MSTRG_multi_exon_lincRNA_locus_map.txt'
    main(input_file, output_file_1, output_file_2)

