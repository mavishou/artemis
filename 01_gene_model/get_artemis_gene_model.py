#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2017/3/8


from myToolKit.general_helper import *
from myToolKit.bio.gtf.gtf_util import processGTFAnno


def get_MSTRG_map(map_file):
    mstrg_to_locus = {}
    with open('MSTRG_multi_exon_lincRNA_locus_map.txt') as reader:
        for line in reader:
            lnc, locus = line_to_list(line)
            mstrg_to_locus[lnc] = locus
    return mstrg_to_locus


def main(JS_gene_model_file, map_file, artemis_gm_file):
    mstrg_to_locus = get_MSTRG_map(map_file)

    with open(JS_gene_model_file) as reader, open(artemis_gm_file, 'w') as writer:
        for line in reader:
            cols = line_to_list(line)
            annotations = processGTFAnno(cols[8])
            trans_id = annotations['transcript_id']
            if trans_id.startswith('MSTRG'):
                if trans_id in mstrg_to_locus:
                    annotation = 'gene_id "{}"; transcript_id "{}"; '.format(mstrg_to_locus[trans_id], trans_id)
                    cols[8] = annotation
                    writer.write(get_output_line(cols))
            else:
                writer.write(line)


if __name__ == '__main__':
    input_file_1 = 'JS_exon_simple_modified.gtf'
    input_file_2 = 'MSTRG_multi_exon_lincRNA_locus_map.txt'
    output_file = 'artemis_exon_unsorted.gtf'
    main(input_file_1, input_file_2, output_file)

