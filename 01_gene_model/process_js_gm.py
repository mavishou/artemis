#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2016/12/15

from myToolKit.general_helper import *
from myToolKit.bio.gtf.gtf_util import get_simple_anno
import re


def main(gene_model_file):
    # set up
    out_all_full = 'JS_all_full.gtf'
    out_all_simple = 'JS_all_simple.gtf'
    out_exon_full = 'JS_exon_full.gtf'
    out_exon_simple = 'JS_exon_simple.gtf'

    p_main_chr = re.compile(r'^[0-9XYM]')
    p_mt = re.compile(r'^(MT)')

    with open(gene_model_file) as reader, \
        open(out_all_full, 'w') as w_all_full, \
        open(out_all_simple, 'w') as w_all_simple, \
        open(out_exon_full, 'w') as w_exon_full, \
        open(out_exon_simple, 'w') as w_exon_simple:

        for line in reader:
            if p_main_chr.search(line):
                line = p_mt.sub('T', line)

                # for all full
                line = 'chr' + line
                w_all_full.write(line)

                # for all simple
                cols = line_to_list(line)
                simple_cols = cols[:]
                simple_cols[8] = get_simple_anno(cols[8])
                simple_line = get_output_line(simple_cols)
                w_all_simple.write(simple_line)

                # for exon
                if cols[2] == 'exon':
                    w_exon_full.write(line)
                    w_exon_simple.write(simple_line)


if __name__ == '__main__':
    js_gene_model = 'JS_gene_model_final.gtf'
    main(js_gene_model)

