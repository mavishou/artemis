#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2017/3/8

from myToolKit.general_helper import *
from myToolKit.bio.gtf.gtf_util import processGTFAnno


def _load_lncRNAs():
    all_lncRNAs = set()
    with open('JS_lncRNAs.txt') as reader:
        for line in reader:
            all_lncRNAs.add(line.rstrip('\n'))

    def inner(trans):
        return trans in all_lncRNAs

    return inner

is_a_lncRNA = _load_lncRNAs()


def main(gene_model_file):
    # set up
    out_gene_model = 'JS_exon_simple_modified.gtf'

    with open(gene_model_file) as reader, \
            open(out_gene_model, 'w') as writer:
        for line in reader:
            cols = line_to_list(line)
            trans_name = processGTFAnno(cols[8])['transcript_id']
            if (not trans_name.startswith('MSTRG')) or (is_a_lncRNA(trans_name)):
                writer.write(line)
        writer.write(get_output_line(['chr8', 'HM', 'exon', '127204774', '127209717', '.', '-', '.', 'gene_id "ENSG00000247844.1"; transcript_id "CCAT1-L"; ']))
        writer.write(get_output_line(['chr8', 'HM', 'exon', '127218810', '127219088', '.', '-', '.', 'gene_id "ENSG00000247844.1"; transcript_id "CCAT1-L"; ']))

if __name__ == '__main__':
    js_exon_gm = 'JS_exon_simple.gtf'
    main(js_exon_gm)

