
# Gene model from JS
## Get the gene model

```bash
# The whole gene model
scp gaog_hm@10.100.1.88:/10.100.1.5/gaog_pkuhpc/home/gaog_pkuhpc/users/jiangs/01_data/02_TCGA/JS_whole_gene_model_Final.gtf ./JS_gene_model_final.gtf
# Expressed lncRNA
scp gaog_hm@10.100.1.88:/10.100.1.5/gaog_pkuhpc/home/gaog_pkuhpc/users/jiangs/01_data/02_TCGA/survival_analysis/01_lme_cox/01_Breast/expressed_anno_lncRNA.ID  ./JS_expressed_lncRNA.txt
# Expressed novel lncRNA
scp gaog_hm@10.100.1.88:/10.100.1.5/gaog_pkuhpc/home/gaog_pkuhpc/users/jiangs/01_data/02_TCGA/survival_analysis/01_lme_cox/01_Breast/expressed_new_lncRNA.ID ./JS_novel_expressed_lncRNA.txt
```
## Process
```bash
cat JS_gene_model_final.gtf | cut -f 3 | uniq | sort | uniq > JS_features.txt
python process_js_gm.py
```

* `JS_all_full.gtf`
* `JS_all_simple.gtf`
* `JS_exon_full.gtf`
* `JS_exon_simple.gtf`

```bash
# get annotation maps
python /lustre/user/houm/projects/myToolKit/bio/gtf/get_anno_features.py JS_exon_full.gtf JS_anno_maps.txt
```
## Get transcript & gene level gene model
```bash
check_sort.py < JS_exon_simple.gtf
# get transcript
get_trans.py < JS_exon_simple.gtf > JS_trans.gtf
# get gene
get_gene.py < JS_trans.gtf > JS_gene.gtf
# build index
(cat JS_gene.gtf | sort -k1,1 -k4,4n) | bgzip > JS_gene_sorted.gtf.gz
tabix -p gff JS_gene_sorted.gtf.gz
```

## For HISAT2
```bash
# for hisat2
python ~/tools/hisat2-2.0.2-beta/hisat2_extract_splice_sites.py JS_exon_simple.gtf > JS_splice_sites.txt
```

# Gene model from GENECODE
Version: v25

```bash
wget ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_25/gencode.v25.annotation.gtf.gz
unzip gencode.v25.annotation.gtf.gz
```

# LncRNA ID
```bash
# On HPC
# known lncRNAs
ln -s /home/gaog_pkuhpc/users/jiangs/01_data/14_data_process_R/normalization/quantile_norm/annotated_lncRNA.ID JS_known_lncRNAs.txt
# Novel lncRNAs
ln -s /home/gaog_pkuhpc/users/jiangs/01_data/14_data_process_R/normalization/quantile_norm/new_transcript_noncoding_gt_200 JS_novel_lncRNAs.txt
cat JS_known_lncRNAs.txt JS_novel_lncRNAs.txt > JS_lncRNAs.txt
wc -l JS_lncRNAs.txt
# 118324
```

# AnnoLnc gene model for hg19
```bash
# for hisat2
python ~/tools/hisat2-2.0.2-beta/hisat2_extract_splice_sites.py ../../AnnoLnc/gene_model/annolnc_v1_exon.gtf > annolnc_splicesites_hg19.txt
```

# Optimize the gene model
## Try 1: merge lncRNAs into locus -> failed

Many transcripts assembled by JS are not lncRNAs. I want to remove these lncRNAs.

```bash
cat JS_lncRNAs.txt | grep ^MSTRG | wc -l
66485
cat JS_trans.gtf | grep MSTRG | wc -l
195733
```

### The process to get lncRNA locus

```bash
echo CCAT1-L >> JS_lncRNAs.txt
python filter_JS_not_lncRNA.py
# check sort
check_sort.py < JS_exon_simple_modified.gtf
# get transcript
get_trans.py < JS_exon_simple_modified.gtf > JS_trans_modified.gtf
# convert to bed
cat JS_trans_modified.gtf | sed -e 's/"//g' -e 's/;//g' | awk '{print $1"\t"$4-1"\t"$5"\t"$12"\t.\t"$7}' > JS_trans_modified.bed
# select lncRNAs
myjoin JS_trans_modified.bed JS_lncRNAs.txt -F 4 | grep ^= | cut -f 2-7 > JS_lncRNA_trans.bed
# sort 
cat JS_lncRNA_trans.bed | sort -k1,1 -k2,2n > JS_lncRNA_trans_sorted.bed
# merge 
mergeBed -i JS_lncRNA_trans_sorted.bed -c 4 -o collapse > JS_lncRNA_merged.bed
# get lncRNA locus
python get_lnc_locus.py
```
I find it's not reliable because the merged regions are too long

## Try 2: merge MSTR lncRNAs only
### Filter MSTR lncRNAs
Requirement: 
1. Not overlapped with other transcripts
2. multi exon

```bash
# separate JS's lncRNA and other transcripts
cat JS_trans_modified.gtf | grep MSTRG > MSTRG_lncRNA_trans.gtf
cat JS_trans_modified.gtf | grep -v MSTRG > others_trans.gtf
# get MSTR intergenic lncRNAs
intersectBed -a MSTRG_lncRNA_trans.gtf -b others_trans.gtf -v > MSTRG_lincRNA_trans.gtf

# convert to bed
cat MSTRG_lincRNA_trans.gtf | sed -e 's/"//g' -e 's/;//g' | awk '{print $1"\t"$4-1"\t"$5"\t"$12"\t.\t"$7}' > MSTRG_lincRNA_trans.bed

# get multi exon MSTRG
cat JS_exon_simple_modified.gtf | grep MSTRG | sed -e 's/"//g' -e 's/;//g' | awk '{print $12}' | sort | uniq -c | awk '$1>1 {print $2}' > MSTRG_multi_exon.txt

# remove single exon
myjoin MSTRG_lincRNA_trans.bed MSTRG_multi_exon.txt -F 4 | grep ^= | cut -f 2-7 > MSTRG_multi_exon_lincRNA_trans.bed
```

### Get MSTR lncRNA locus
```
# sort
cat MSTRG_multi_exon_lincRNA_trans.bed | sort -k1,1 -k2,2n > MSTRG_multi_exon_lincRNA_trans_sorted.bed
# merge
mergeBed -i MSTRG_multi_exon_lincRNA_trans_sorted.bed -c 4 -o collapse -s > MSTRG_multi_exon_lincRNA_merged.bed
# get locus
python get_lnc_locus.py
```

### Get artemis gene model
```bash
# get gene model
python get_artemis_gene_model.py
# sort
cat artemis_exon_unsorted.gtf | sort -k1,1 -k10,10 -k12,12 -k4,4n > artemis_exon.gtf

# check sort
check_sort.py < artemis_exon.gtf
# get transcript
get_trans.py < artemis_exon.gtf > artemis_trans.gtf
cat artemis_trans.gtf | sed -e 's/"//g' -e 's/;//g' | awk '{print $12"\t"$10}' | uniq > artemis_anno_map.txt
myjoin artemis_anno_map.txt  JS_lncRNAs.txt | grep ^= | cut -f 2 > artemis_lncRNAs.txt

# get gene
get_gene.py < artemis_trans.gtf > artemis_gene.gtf
# build index
(cat artemis_gene.gtf | sort -k1,1 -k4,4n) | bgzip > artemis_gene_sorted.gtf.gz
tabix -p gff artemis_gene_sorted.gtf.gz

# gene bed for tabix
(cat artemis_gene.bed | sort -k1,1 -k2,2n) | bgzip > artemis_gene_sorted.bed.gz
tabix -p bed artemis_gene_sorted.bed.gz
```

```bash
myjoin -F 4 artemis_gene.bed PCG.txt | grep ^= | cut -f 2-7 > PCG_gene.bed

# lncRNA not overlap with PCG
intersectBed -a artemis_gene_lnc.bed -b PCG_gene.bed -v > lnc_not_overlap_PCG.bed
# ENSG
cat lnc_not_overlap_PCG.bed | awk '$4~/^ENSG/' > lnc_ENSG_not_overlap_PCG.bed
# Not ENSG
cat lnc_not_overlap_PCG.bed | awk '$4!~/^ENSG/' > lnc_not_ENSG_not_overlap_PCG.bed
# Not ENSG not overlap with ENSG
intersectBed -a lnc_not_ENSG_not_overlap_PCG.bed -b lnc_ENSG_not_overlap_PCG.bed -v > lnc_NOT_ENSG_NOT_overlap_ENSG.bed

cat lnc_ENSG_not_overlap_PCG.bed lnc_NOT_ENSG_NOT_overlap_ENSG.bed > lnc_to_report.bed
```


