#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
run_dir <- args[1]
# run_dir <- 'run/ENST00000496488.1'

options(stringsAsFactors = F)

norm_exp <- read.table('../03_cal_exp/norm_exp_simple.txt', sep = '\t')

sample_info <- read.table('../03_cal_exp/sample_info.txt', sep = '\t')
colnames(sample_info) <- c('sample', 'cell_line')

tb_gene_trans <- read.table(paste0(run_dir, '/overlapped_trans.txt'), sep = '\t')
colnames(tb_gene_trans) <- c('gene', 'trans')
genes <- tb_gene_trans$gene

trans_norm_exp <- norm_exp[tb_gene_trans$trans, ]
gene_norm_exp <- apply(trans_norm_exp, 2, function(x) {
  tapply(x, as.factor(genes), sum)
})

merge_exp <- function(tb_exp) {
  result <- sapply(as.list(seq(1, ncol(tb_exp), 2)), function(x) {
    m <- (tb_exp[, x] + tb_exp[, x + 1]) / 2
    return(round(m, 4))
  })
  colnames(result) <- unique(sample_info$cell_line)
  rownames(result) <- rownames(tb_exp)
  return(result)
}

trans_exp <- merge_exp(trans_norm_exp)
gene_exp <- merge_exp(gene_norm_exp)

write.table(trans_norm_exp, paste0(run_dir, '/trans_norm.exp'), sep = '\t', quote = F)
write.table(gene_norm_exp, paste0(run_dir, '/gene_norm.exp'), sep = '\t', quote = F)
write.table(trans_exp, paste0(run_dir, '/trans.exp'), sep = '\t', quote = F)
write.table(gene_exp, paste0(run_dir, '/gene.exp'), sep = '\t', quote = F)

near_genes <- scan(paste0(run_dir, '/near_genes.txt'), what = 'characters')

near_gene_exp <- gene_exp[near_genes, ]
write.table(near_gene_exp, paste0(run_dir, '/near_gene.exp'), sep = '\t', quote = F)
