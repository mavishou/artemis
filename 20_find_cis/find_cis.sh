#!/usr/bin/env bash

set -o errexit
set -o nounset

ID=$1
JS_TRANS_GTF=../01_gene_model/JS_trans.gtf
JS_GENE_GTF=../01_gene_model/JS_gene_sorted.gtf.gz
JS_ANNO_MAP=../01_gene_model/JS_anno_maps.txt
CELL_LINE_INFO=../00_samples/4_cell_lines
EXPAND=1000000

prepare() {
    echo "Prepare"
    RUN_DIR=run/$ID
    mkdir -p $RUN_DIR
    TRANS_GTF=$RUN_DIR/trans.gtf
    OVERLAPPED_GENE_GTF=$RUN_DIR/overlapped_genes.gtf
    OVERLAPPED_GENES=$RUN_DIR/overlapped_genes.txt
    NEAR_GENES=$RUN_DIR/near_genes.txt
    OVERLAPPED_TRANS=$RUN_DIR/overlapped_trans.txt
}

get_lnc_trans_gtf() {
    echo "Get trans GTF of the lncRNA"
    cat ../01_gene_model/JS_trans.gtf | gawk -v trans="$ID" '$12=="\""trans"\";"' > $TRANS_GTF
    if [[ ! -s $TRANS_GTF ]]; then 
        echo "No transcript find in $JS_TRANS_GTF"
        exit 1
    fi
}

get_flank_region() {
    echo "Get flank region"
    read CHR START END <<< $(cat $TRANS_GTF | cut -f 1,4,5)
    echo $CHR:$START-$END
    START=$(($START-$EXPAND))
    if [[ $START -lt 1 ]]; then START=1; fi
    END=$(($END+$EXPAND))
    REGION=$CHR:$START-$END
    echo "Flank region: $REGION"
}

get_overlapped_genes() {
    echo "Get overlapped genes"
    tabix $JS_GENE_GTF $REGION > $OVERLAPPED_GENE_GTF
    cat $OVERLAPPED_GENE_GTF | gawk '{print $10}' | sed 's/"//g' | sed 's/;//' > $OVERLAPPED_GENES
}

_get_lnc_gene() {
    GENE_ID=$(cat $TRANS_GTF | gawk '{print $10}' | sed 's/"//g' | sed 's/;//')
    echo "Gene ID of lncRNA: $GENE_ID"
}

get_near_genes() {
    echo "Get near genes"
    _get_lnc_gene
    cat $OVERLAPPED_GENES | grep -w "$GENE_ID" -3 > $NEAR_GENES
}

get_overlapped_trans() {
    echo "Get overlapped transcripts"
    myjoin $OVERLAPPED_GENES $JS_ANNO_MAP | grep ^= | cut -f 3,5 > $OVERLAPPED_TRANS
}

extract_exp() {
    echo "Extract expression"
    Rscript extract_exp.R $RUN_DIR
}

get_overlapped_promoter_enhancer() {
    echo "Get overlapped promoters and enhancers"
    cat $CELL_LINE_INFO | while read cell_type; do
        CELL_DIR=$RUN_DIR/$cell_type
        mkdir -p $CELL_DIR
        for anno_type in $(echo "promoter enhancer"); do
            local overlapped_anno_file=$CELL_DIR/$anno_type.bed
            local bed_file=../04_encode_annotation/data/${cell_type}_${anno_type}_hg38.bed.sorted.gz
            tabix $bed_file $REGION > $overlapped_anno_file
        done
    done
}

get_loop_peaks() {
    echo "Get Hi-C loop peaks"
    cat $CELL_LINE_INFO | while read cell_type; do
        CELL_DIR=$RUN_DIR/$cell_type
        mkdir -p $CELL_DIR
        local bed_file=../05_hic_loops/data/${cell_type}_peaks_hg38.bed.sorted.gz
        local overlapped_peak_file=$CELL_DIR/peaks.bed
        tabix $bed_file $REGION > $overlapped_peak_file
    done
}

_get_intersect() {
    PROMOTER_BED=$CELL_DIR/promoter.bed
    ENHANCER_BED=$CELL_DIR/enhancer.bed
    PEAK_BED=$CELL_DIR/peaks.bed
    intersectBed -a $PEAK_BED -b $PROMOTER_BED -wb > $CELL_DIR/promoter_peaks.intersect
    intersectBed -a $PEAK_BED -b $ENHANCER_BED -wb > $CELL_DIR/enhancer_peaks.intersect
}

get_loop_details() {
    echo "Get loop details"
    cat $CELL_LINE_INFO | while read cell_type; do
        CELL_DIR=$RUN_DIR/$cell_type
        mkdir -p $CELL_DIR
        _get_intersect
        cmd="python2.7 get_loop_details.py $CELL_DIR"
        echo $cmd
        eval $cmd
    done
    
}

main() {
    echo $ID
    prepare
    get_lnc_trans_gtf
    get_flank_region
    get_overlapped_genes
    get_near_genes
    get_overlapped_trans
    extract_exp
    get_overlapped_promoter_enhancer
    get_loop_peaks
    get_loop_details
}

main
