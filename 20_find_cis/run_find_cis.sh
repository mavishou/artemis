#!/usr/bin/env bash

set -o errexit
set -o nounset

source ~/toolkit/general_func.sh

prepare() {
    LOG_DIR=logs
    mkdir -p $LOG_DIR
    local now=$(date +%y%m%d-%H%M%S)
    LOG_FILE=$LOG_DIR/$now-find_cis.log
}

run_find_cis() {
    local cmd="bash find_cis.sh $ID"
    run_cmd "$cmd" "run_find_cis"
}

main() {
    prepare
    (time while read ID; do
        echo $ID
        run_find_cis $ID
        echo "========================================================"
    done) 2>&1 | tee $LOG_FILE
}

main
