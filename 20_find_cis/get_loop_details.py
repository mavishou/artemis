#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2016/12/30

import argparse
from myToolKit.general_helper import *
from myToolKit.util.decorators import *


class GetLoopDetail(object):

    def __init__(self, cell_dir):
        self.cell_dir = cell_dir
        self.peaks = {}

    @print_func_name
    def get_all_peaks(self):
        # logging.info('Get all peaks')
        peak_file = os.path.join(self.cell_dir, 'peaks.bed')
        with open(peak_file) as reader:
            for line in reader:
                chr, start, end, name = line_to_list(line)
                peak = Peak(name, int(start), int(end))
                self.peaks[peak.name] = peak
                # print peak.name

    @print_func_name
    def get_promoter_intersection(self):
        intersect_file = os.path.join(self.cell_dir, 'promoter_peaks.intersect')
        with open(intersect_file) as reader:
            for line in reader:
                cols = line_to_list(line)
                peak_name = cols[3]
                annotation = Annotation(cols[7], int(cols[5]), int(cols[6]), int(cols[1]), int(cols[2]))
                self.peaks[peak_name].promoters.add(annotation)

    @print_func_name
    def get_enhancer_intersection(self):
        intersect_file = os.path.join(self.cell_dir, 'enhancer_peaks.intersect')
        with open(intersect_file) as reader:
            for line in reader:
                cols = line_to_list(line)
                peak_name = cols[3]
                annotation = Annotation(cols[7], int(cols[5]), int(cols[6]), int(cols[1]), int(cols[2]))
                self.peaks[peak_name].enhancers.add(annotation)

    @print_func_name
    def get_loops(self):

        def _get_output(peak_name):
            if peak_name in self.peaks:
                peak = self.peaks[peak_name]
                return [peak_name, str(peak.length), peak.output_promoters(), peak.output_enhancers()]
            else:
                return [peak_name, '-', '-', '-']

        cell_type = os.path.basename(self.cell_dir)
        loops_file = os.path.join('../05_hic_loops/data', '{}_loops.txt'.format(cell_type))
        output_file = os.path.join(self.cell_dir, 'loops.txt')

        with open(loops_file) as reader, open(output_file, 'w') as writer:
            reader.readline()
            writer.write(get_output_line(['peak_name_1', 'peak_len_1', 'overlapped_promoters_1', 'overlapped_enhancers_1',
                                          'peak_name_2', 'peak_len_2', 'overlapped_promoters_2', 'overlapped_enhancers_2']))
            for line in reader:
                up, down, inteval = line_to_list(line)
                if up in self.peaks or down in self.peaks:
                    l_out = _get_output(up)
                    l_out += _get_output(down)
                    writer.write(get_output_line(l_out))


class Region(object):

    def __init__(self, name, start, end):
        self.name = name
        self.start = start
        self.end = end
        self._get_length()

    def _get_length(self):
        self.length = self.end - self.start

    def __hash__(self):
        return hash(self.name)

    def __cmp__(self, other):
        return self.name == other.name


def _get_output_annotations(annotations):
    l_out = [str(a) for a in annotations]
    if len(l_out) != 0:
        result = ' | '.join(l_out)
    else:
        result = '-'
    return result


class Peak(Region):

    def __init__(self, name, start, end):
        super(Peak, self).__init__(name, start, end)
        self.promoters = set()
        self.enhancers = set()

    def output_promoters(self):
        return _get_output_annotations(self.promoters)

    def output_enhancers(self):
        return _get_output_annotations(self.enhancers)


class Annotation(Region):

    def __init__(self, name, start, end, i_start, i_end):
        super(Annotation, self).__init__(name, start, end)
        self.i_start = i_start
        self.i_end = i_end
        self._get_intersect_length()
        self._get_intersect_rate()

    def _get_intersect_length(self):
        self.i_length = self.i_end - self.i_start

    def _get_intersect_rate(self):
        self.i_rate = round(float(self.i_length) / self.length, 2)

    def __str__(self):
        return '{}({};{};{})'.format(self.name, self.length, self.i_length, self.i_rate)


@print_both_single_line
def main(cell_dir):
    get_loop_detail = GetLoopDetail(cell_dir)
    get_loop_detail.get_all_peaks()
    get_loop_detail.get_promoter_intersection()
    get_loop_detail.get_enhancer_intersection()
    get_loop_detail.get_loops()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('cell_dir')

    args = parser.parse_args()

    main(args.cell_dir)
