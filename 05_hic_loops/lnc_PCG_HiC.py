#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2017/3/12


import argparse
from collections import defaultdict
from myToolKit.general_helper import *


def get_peaks_of_genes(gene_peak_neighbor_file):
    gene_loop_peaks = defaultdict(lambda: defaultdict(set))
    with open(gene_peak_neighbor_file) as reader:
        for line in reader:
            cols = line_to_list(line)
            gene = cols[3]
            peak = cols[9]
            loop = peak.split('_')[0]
            gene_loop_peaks[gene][loop].add(peak)
    return gene_loop_peaks


def main(gene_peak_neighbor_file, lnc_PCG_pair_file, output_file):
    gene_loop_peaks = get_peaks_of_genes(gene_peak_neighbor_file)

    with open(lnc_PCG_pair_file) as reader, open(output_file, 'w') as writer:
        reader.readline()
        writer.write(get_output_line(['lnc', 'lnc_loop_peak', 'PCG', 'PCG_loop_peak']))

        for line in reader:
            cols = line_to_list(line)
            lnc_id = cols[0]
            pcg_id = cols[1]
            lnc_loops = gene_loop_peaks[lnc_id]
            lnc_loop_names = set(lnc_loops.keys())
            pcg_loops = gene_loop_peaks[pcg_id]
            pcg_loop_names = set(pcg_loops.keys())

            common_loop_names = lnc_loop_names & pcg_loop_names
            if len(common_loop_names) >= 1:
                for loop_name in common_loop_names:
                    lnc_peaks = lnc_loops[loop_name]
                    pcg_peaks = pcg_loops[loop_name]
                    for lnc_peak in lnc_peaks:
                        for pcg_peak in pcg_peaks:
                            if lnc_peak != pcg_peak:
                                writer.write(get_output_line([lnc_id, lnc_peak, pcg_id, pcg_peak]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('gene_peak_neighbor_file')
    parser.add_argument('lnc_PCG_pair_file')
    parser.add_argument('output_file')

    args = parser.parse_args()

    main(args.gene_peak_neighbor_file, args.lnc_PCG_pair_file, args.output_file)
