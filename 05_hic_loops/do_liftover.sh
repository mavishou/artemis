#!/usr/bin/env bash

set -o errexit
set -o nounset

prepare_vars() {
    OLD_FILE=data/${CELL_LINE}_peaks_hg19.bed
    NEW_FILE=data/${CELL_LINE}_peaks_hg38.bed
    UNMAP_FILE=data/${CELL_LINE}_unmapped.bed
}

do_liftover() {
    local cmd="liftOver $OLD_FILE $CHAIN_FILE $NEW_FILE $UNMAP_FILE"
    echo "$cmd"
    eval $cmd
}

main() {
    CHAIN_FILE=/lustre/user/houm/lift_over_chain/hg19ToHg38.over.chain
    while read CELL_LINE; do
        prepare_vars
        do_liftover
    done
}

main
