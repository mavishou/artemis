#!/usr/bin/env bash
# run on Solar

set -o nounset
set -o errexit

prepare() {
    DIR=cell_lines/$CELL_TYPE
    mkdir -p $DIR
    GENE_GTF=../01_gene_model/artemis_gene.bed
    LNC_PCG_PAIR_FILE=../21_lnc_neighbor/lnc_PCG_filtered.txt
    HIC_PEAK=results/${CELL_TYPE}_loop_peaks_hg38.bed
    GENE_HIC_PEAK=$DIR/gene_neighbor_hic_peaks.txt
    PAIR_HIC=$DIR/lnc_PCG_HiC.txt
}

get_genes_neighbor_hic_peaks() {
    windowBed -a $GENE_GTF -b $HIC_PEAK -w 3000 > $GENE_HIC_PEAK
}

add_HiC_peaks_for_PCG_lnc_pairs() {
    /usr/local/bin/python2.7 lnc_PCG_HiC.py $GENE_HIC_PEAK $LNC_PCG_PAIR_FILE $PAIR_HIC
}

main() {
    while read CELL_TYPE; do
        echo $CELL_TYPE
        prepare
        get_genes_neighbor_hic_peaks
        add_HiC_peaks_for_PCG_lnc_pairs
    done
}

main