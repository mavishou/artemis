# Datasets
* Downloaded from [GSE63525](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE63525)
* Six cell lines: See `../00_samples/HiC_cell_lines.txt`
* Raw data: `raw_data`

# Process raw data
```bash
cat ../00_samples/HiC_cell_lines.txt | while read c; do Rscript process_raw_data.R $c; done
```

# liftOver
```bash
# do liftover
cat ../00_samples/HiC_cell_lines.txt | bash do_liftover.sh

# Check unmapped
cd data/
for um in $(ls *_unmapped.bed); do grep -c "^chr" $um ; done
7
2
1
8
4
4
```

# Prepare for tabix
```
cd results
for f in $(ls *_loop_peaks_hg38.bed); do (cat $f | sort -k1,1 -k2,2n) | bgzip > $f.sorted.gz; tabix -p bed $f.sorted.gz; done
```

# For virsulize
```bash
# Convert to GTF
cat ../00_samples/HiC_cell_lines.txt | while read c; do cat results/${c}_loop_peaks_hg38.bed | sed -r 's/(L[0-9]+)_[0-9]/\1/' | awk '{print $1"\tHiC\texon\t"$2+1"\t"$3"\t"$5"\t.\t.\tgene_id \""$4"\"; transcript_id \""$4"\"; "}' > for_visualize/${c}_loop_peaks.gtf; get_gene.py < for_visualize/${c}_loop_peaks.gtf > for_visualize/${c}_loop_gene.gtf; done


# for tabix
cd for_visualize
cat ../../00_samples/HiC_cell_lines.txt | while read c; do f=${c}_loop_gene.gtf; (cat $f | sort -k1,1 -k4,4n) | bgzip > $f.sorted.gz; tabix -p gff $f.sorted.gz; done
cat ../../00_samples/HiC_cell_lines.txt | while read c; do f=${c}_loop_peaks.gtf; (cat $f | sort -k1,1 -k4,4n) | bgzip > $f.sorted.gz; tabix -p gff $f.sorted.gz; done
```


