Download the lncRNA-disease file from http://www.bio-bigdata.net/lnc2cancer/down.jsp

```bash
cat lnc2cancer_filtered.txt | sed '1 d' | awk -F "\t" '{print $2"\n"$3}' | grep -v "N/A" | sort -u | sed '1 d' > lnc2cancer_IDs.txt

cat ../01_gene_model/artemis_gene.bed | cut -f 4 > tmp1
cat tmp1 | sed -r 's/^(.+)\.[0-9]+$/\1/' > tmp2
paste tmp2 tmp1 > tmp3
myjoin tmp3 lnc2cancer_IDs.txt | grep ^= | cut -f 2,3 > ID_map.txt
rm -f tmp1 tmp2 tmp3

# stat
cat high_conf_cancer_related.txt | sed '1 d' | wc -l
119
cat high_conf_cancer_related.txt | sed '1 d' | cut -f 3 | sort -u | wc -l
29
```



