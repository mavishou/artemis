The idea is to find this kind of lncRNAs: find these kind of lncRNAs: it can influence the interaction of nearby promoters and enhancers, thus regulate nearby genes.

# Prepare data
## Enhancer & promoter data from ENCODE
See `04_encode_annotation`
### Download 
```bash
cat to_download.txt | bash download.sh
```
### liftOver
```bash
cat to_download.txt | bash do_liftover.sh
# Check unmapped
for um in $(ls *_unmapped.bed); do grep -c "^chr" $um ; done
17
6
16
8
7
5
20
5
```
### prepare for tabix
```
cd data
for f in $(ls *_hg38.bed); do (cat $f | sort -k1,1 -k2,2n) | bgzip > $f.sorted.gz; tabix -p bed $f.sorted.gz; done
```

## Hi-C loops data
See `05_hic_loops`
### liftOver
```bash
# prepare
cat ../00_samples/4_cell_lines | while read c; do Rscript for_liftover.R $c; done
# do liftover
cat ../00_samples/4_cell_lines | bash do_liftover.sh
# Check unmapped
for um in $(ls *_unmapped.bed); do grep -c "^chr" $um ; done
6
2
6
4
# post process
cat ../00_samples/4_cell_lines | while read c; do Rscript after_liftover.R $c; done
```
### prepare for tabix
```
cd data
for f in $(ls *_peaks_hg38.bed); do (cat $f | sort -k1,1 -k2,2n) | bgzip > $f.sorted.gz; tabix -p bed $f.sorted.gz; done
```

## Expresson data
See `03_cal_exp`

## Find cis
```bash
cat ../03_cal_exp/expressed_lncRNAs.txt | sed -n '1,1455p' | bash run_find_cis.sh
cat ../03_cal_exp/expressed_lncRNAs.txt | sed -n '1456,2910p' | bash run_find_cis.sh
cat ../03_cal_exp/expressed_lncRNAs.txt | sed -n '2911,4365p' | bash run_find_cis.sh
cat ../03_cal_exp/expressed_lncRNAs.txt | sed -n '4966,5820p' | bash run_find_cis.sh
cat ../03_cal_exp/expressed_lncRNAs.txt | sed -n '5821,7275p' | bash run_find_cis.sh
```



