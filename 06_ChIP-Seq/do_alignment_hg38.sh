#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1
SRRS=$2
CPU_NUM=5

_get_fastq() {
    local srrs=$1
    local fastq_files=$(echo $srrs | tr "," "\n" | sed 's:^:../00_samples/fastq/:' | sed "s/$/.fastq.bz2/" | tr "\n" "," | sed 's/,$//')
    echo $fastq_files
}

prepare_target_dir() {
    TARGET_DIR=alignment_hg38/$ID
    mkdir -p $TARGET_DIR
}

_prepare_bowtie2() {
    GENOME=/home/gaog_pkuhpc/users/houm/genome/human/hg38/bowtie2/genome
    OUT_SAM=$TARGET_DIR/all.sam
    FQ_FILE=$(_get_fastq $SRRS)
}

_make_bowtie2_cmd() {
    CMD_BW2="~/users/houm/tools/bowtie2-2.3.0-legacy/bowtie2"
    CMD_BW2="$CMD_BW2 -x $GENOME"
    CMD_BW2="$CMD_BW2 -U $FQ_FILE"
    CMD_BW2="$CMD_BW2 -S $OUT_SAM"
    CMD_BW2="$CMD_BW2 -p $CPU_NUM"
}

run_bowtie2() {
    _prepare_bowtie2
    _make_bowtie2_cmd
    run_cmd "$CMD_BW2" "Running bowtie2"
}

_remove_tmp_sam() {
    if [ -s $UNIQUE_BAM ]; then rm -f $OUT_SAM; fi
}

filter_reads_and_convert_to_bam() {
    UNIQUE_BAM=$TARGET_DIR/high_quality.bam
    local cmd="~/users/houm/tools/samtools-1.3.1/bin/samtools view -@ $CPU_NUM -q 10 -b $OUT_SAM > $UNIQUE_BAM"
    run_cmd "$cmd" "Filtering reads"
    _remove_tmp_sam
}

_remove_tmp_bam() {
    if [ -s $SORT_BAM ]; then rm -f $UNIQUE_BAM; fi
}

run_sort() {
    SORT_BAM=$TARGET_DIR/high_quality.sort.bam
    local cmd="~/users/houm/tools/samtools-1.3.1/bin/samtools sort -@ $CPU_NUM -o $SORT_BAM $UNIQUE_BAM"
    run_cmd "$cmd" "Sorting bam file"
    _remove_tmp_bam
}

build_bam_index() {
    TARGET_BAM=$SORT_BAM
    if [[ ! -s $TARGET_BAM ]]; then
        echo "$TARGET_BAM is not existed."
        exit 1
    fi
    local cmd="~/users/houm/tools/samtools-1.3.1/bin/samtools index $TARGET_BAM"
    run_cmd "$cmd" "Building index"
}

remove_dup_reads() {
    local dedup_file=$TARGET_DIR/dedupped.sort.bam
    cmd="~/users/houm/tools/samtools-1.3.1/samtools rmdup -s $SORT_BAM $dedup_file"
    run_cmd "$cmd" "Remove duplicate reads"
}

main() {
    echo $ID
    prepare_target_dir
    run_bowtie2
    filter_reads_and_convert_to_bam
    run_sort
    build_bam_index
    remove_dup_reads
    echo "Done!"
}

main
