#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

SRR=$1

prepare_target_dir() {
    TARGET_DIR=raw
    mkdir -p $TARGET_DIR
}

do_fastq_dump() {
    cd raw
    local cmd="fastq-dump --bzip2 --split-3 $SRR.sra"
    run_cmd "$cmd" "Running fastq_dump"
    cd ../
}

main(){
    prepare_target_dir
    do_fastq_dump
}

main
