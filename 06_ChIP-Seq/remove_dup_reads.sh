#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1

TARGET_DIR=alignment_hg38/$ID

dedup() {
    local bam_file=$TARGET_DIR/high_quality.sort.bam
    local dedup_file=$TARGET_DIR/dedupped.sort.bam
    cmd="~/users/houm/tools/samtools-1.3.1/samtools rmdup -s $bam_file $dedup_file"
#    cmd="java -jar ~/users/houm/tools/picard-2.8.2.jar MarkDuplicates I=$bam_file O=$MARK_DUP M=$metrics_file"
    run_cmd "$cmd" "Remove duplicate reads"
}

main() {
    echo $ID
    dedup
}

main
