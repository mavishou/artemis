#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1
TREAT=$2
CONTROL=$3

_get_aln_fils() {
    local ids=$1
    local aln_files=$(echo $ids | tr "," "\n" | sed 's/^/alignment_hg38\//' | sed "s/$/\/high_quality.sort.bam/" | tr "\n" " " | sed 's/ $//')
    echo $aln_files
}

_prepare_callpeak_param() {
    TREAT_FILES=$(_get_aln_fils $TREAT)
    CONTROL_FILES=$(_get_aln_fils $CONTROL)
    DIR=call_peak/$ID
    mkdir -p $DIR
}

_make_callpeak_cmd() {
    CMD_MACS2="~/users/tianf/tools/macs2 callpeak"
    CMD_MACS2="$CMD_MACS2 -t $TREAT_FILES"
    CMD_MACS2="$CMD_MACS2 -c $CONTROL_FILES"
    CMD_MACS2="$CMD_MACS2 -n macs2"
    CMD_MACS2="$CMD_MACS2 --outdir $DIR"
    CMD_MACS2="$CMD_MACS2 -f BAM"
    # CMD_MACS2="$CMD_MACS2 --broad"
    CMD_MACS2="$CMD_MACS2 -g hs" # default
    # CMD_MACS2="$CMD_MACS2 --broad-cutoff 0.1"
    CMD_MACS2="$CMD_MACS2 -q 0.01"
    CMD_MACS2="$CMD_MACS2 -B"
    CMD_MACS2="$CMD_MACS2 --cutoff-analysis"
}

callpeak() {
    _prepare_callpeak_param
    _make_callpeak_cmd
    run_cmd "$CMD_MACS2" "Running MACS2 callpeak"
}

_prepare_cmp_param() {
    TREAT_BDG=$DIR/macs2_treat_pileup.bdg
    CONTROL_BDG=$DIR/macs2_control_lambda.bdg
    CMP_METHOD="ppois FE"
}

_make_cmp_cmd() {
    CMD_CMP="~/users/tianf/tools/macs2 bdgcmp"
    CMD_CMP="$CMD_CMP -t $TREAT_BDG"
    CMD_CMP="$CMD_CMP -c $CONTROL_BDG"
    CMD_CMP="$CMD_CMP -m $CMP_METHOD"
    CMD_CMP="$CMD_CMP --outdir $DIR"
    CMD_CMP="$CMD_CMP --o-prefix macs2"
}

generate_compare_bdg() {
    _prepare_cmp_param
    _make_cmp_cmd
    run_cmd "$CMD_CMP" "Running MACS2 bdgcmp"
}

_sort_bedGraph() {
    local in_file=$1
    local out_file=$2
    local cmd="cat $in_file | sort -k1,1 -k2,2n > $out_file"
    run_cmd "$cmd" "Sorting $in_file"
}

_bedGraph_to_bigWig() {
    local bd_file=$1
    local bd_sort_file=$bd_file.sort
    _sort_bedGraph $bd_file $bd_sort_file

    local bw_file=$2
    local chrom_size_file=/lustre1/gaog_pkuhpc/users/houm/genome/human/hg38/hg38.chrom.sizes
    local cmd="bedGraphToBigWig $bd_sort_file $chrom_size_file $bw_file"
    run_cmd "$cmd" "Converting $bd_file to $bw_file"
}

_prepare_to_convert() {
    FE_BDG=$DIR/macs2_FE.bdg
    FE_BW=$DIR/${ID}_fold_change.bigWig
    PVALUE_BDG=$DIR/macs2_ppois.bdg
    PVALUE_BW=$DIR/${ID}_pvalue.bigWig
}

convert_bedGraph_to_bigWig() {
    _prepare_to_convert
    _bedGraph_to_bigWig $FE_BDG $FE_BW
    _bedGraph_to_bigWig $PVALUE_BDG $PVALUE_BW
}

remove_temp_files() {
    rm -f $FE_BDG
    rm -f $FE_BDG.sort
    rm -f $PVALUE_BDG
    rm -f $PVALUE_BDG.sort
}

main(){
    callpeak
    generate_compare_bdg
    convert_bedGraph_to_bigWig
    remove_temp_files
}

main

