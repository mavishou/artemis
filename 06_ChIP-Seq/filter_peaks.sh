#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1
SPS=$2

prepare() {
    FILTER_FILE_NAME=filtered_by_cutoff.bed
}

_filter_by_cutoff() {
    echo "Filter by cutoff 0.01"
    narrow_peak=call_peak/$sp/macs2_peaks.narrowPeak
    filter_peak=call_peak/$sp/$FILTER_FILE_NAME
    cat $narrow_peak | awk '$9>=2' | cut -f 1,2,3,4,9 > $filter_peak
}


filter_by_cutoff() {
    for sp in $(echo $SPS | sed 's/,/ /g'); do
        _filter_by_cutoff
    done
}

_filter_by_overlapp() {
    echo "Filter by overlapped"
    result_overlap=call_peak/$current/${current}_${other}_overlapped.bed
    other_peak=call_peak/$other/$FILTER_FILE_NAME
    intersectBed -a $current_overlap -b $other_peak -c | awk '$6>0' | cut -f 1,2,3,4,5 > $result_overlap
    current_overlap=$result_overlap
}

filter_by_overlapp() {
    for current in $(echo $SPS | sed 's/,/ /g'); do
        current_overlap=call_peak/$current/$FILTER_FILE_NAME
        final_overlap=call_peak/$current/overlapped.bed
        for other in $(echo $SPS | sed 's/,/ /g'); do
            if [[ $current != $other ]]; then
                _filter_by_overlapp
            fi
        done
        mv $current_overlap $final_overlap
    done
}

main() {
    echo "$ID $SPS"
    prepare
    filter_by_cutoff
    filter_by_overlapp
    echo "Done!"
}

main
