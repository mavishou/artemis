#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1
SRRS=$2
CPU_NUM=10

_get_fastq() {
    local srrs=$1
    local fastq_files=$(echo $srrs | tr "," "\n" | sed 's/^/raw\//' | sed "s/$/.fastq.bz2/" | tr "\n" "," | sed 's/,$//')
    echo $fastq_files
}

prepare_target_dir() {
    TARGET_DIR=alignment/$ID
    mkdir -p $TARGET_DIR
}

_prepare_bowtie2() {
    GENOME=/home/gaog_pkuhpc/users/houm/genome/human/hg19/bowtie2/hg19
    OUT_SAM=$TARGET_DIR/all.sam
    FQ_FILE=$(_get_fastq $SRRS)
}

_make_bowtie2_cmd() {
    CMD_BW2="~/users/houm/tools/bowtie2-2.3.0-legacy/bowtie2"
    CMD_BW2="$CMD_BW2 -x $GENOME"
    CMD_BW2="$CMD_BW2 -U $FQ_FILE"
    CMD_BW2="$CMD_BW2 -S $OUT_SAM"
    CMD_BW2="$CMD_BW2 -p $CPU_NUM"
}

run_bowtie2() {
    _prepare_bowtie2
    _make_bowtie2_cmd
    run_cmd "$CMD_BW2" "Running bowtie2"
}

_remove_tmp_sam() {
    if [ -s $UNIQUE_BAM ]; then rm -f $OUT_SAM; fi
}

filter_reads_and_convert_to_bam() {
    UNIQUE_BAM=$TARGET_DIR/high_quality.bam
    local cmd="samtools view -@ $CPU_NUM -q 10 -b $OUT_SAM > $UNIQUE_BAM"
    run_cmd "$cmd" "Filtering reads"
    _remove_tmp_sam
}

_remove_tmp_bam() {
    if [ -s $UNIQUE_BAM_PREFIX.bam ]; then rm -f $UNIQUE_BAM; fi
}

run_sort() {
    UNIQUE_BAM_PREFIX=$TARGET_DIR/high_quality.sort
    local cmd="samtools sort -@ $CPU_NUM $UNIQUE_BAM $UNIQUE_BAM_PREFIX"
    run_cmd "$cmd" "Sorting bam file"
    _remove_tmp_bam
}

build_bam_index() {
    TARGET_BAM=$TARGET_DIR/high_quality.sort.bam
    if [[ ! -s $TARGET_BAM ]]; then
        echo "$TARGET_BAM is not existed."
        exit 1
    fi
    local cmd="samtools index $TARGET_BAM"
    run_cmd "$cmd" "Building index"
}

main() {
    echo $ID
    prepare_target_dir
    run_bowtie2
    filter_reads_and_convert_to_bam
    run_sort
    build_bam_index
}

main
