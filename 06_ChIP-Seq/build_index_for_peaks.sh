#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1

prepare() {
    TARGET_FILE=call_peak/$ID/overlapped.bed
    GZ_FILE=$TARGET_FILE.sorted.gz
}

prepare_for_tabix() {
    (cat $TARGET_FILE | sort -k1,1 -k2,2n) | bgzip > $GZ_FILE
    tabix -p bed $GZ_FILE
}

main() {
    echo $ID
    prepare
    prepare_for_tabix
    echo "Done!"
}

main
