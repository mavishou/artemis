# Expression from JS
## Overview
```bash
# get expression matrix
ln -s /lustre/user/chengsj/21_TFBS_annotation_Project/new_computation/GTEx/expression_from_jiangs/01_expression/normal_TCGA_FPKM exp_from_js.txt

# How many samples?
cat exp_from_js.txt | head -1 | sed 's/\t/\n/g' | sed '1 d' > all_samples
cat all_samples | wc -l
# 9726

# How many GTEx samples?
cat all_samples | grep "^SRR" | wc -l
# 7849
```
* Total samples: 9726
* GTEx samples: 7849
* TCGA samples: 1877

# Build database
## Prepare other info
```bash
# get the TCGA sample info
scp gaog_hm@10.100.1.88:/10.100.1.5/gaog_pkuhpc/home/gaog_pkuhpc/users/jiangs/01_data/02_TCGA/Coverage_result/TCGA_info_I_need TCGA_sample_info
cat all_samples | cut -d ':' -f 1 > header_for_db
cat exp_from_js.txt | cut -f 1 | sed '1 d' > all_trans
```
See `prepare_info.R`

## Prepare expression
```bash
python get_exp_for_db.py exp_from_js.txt exp_for_db.txt
```

## Insert to database
See `exp.sql`


