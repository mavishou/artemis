setwd('/lustre/user/houm/projects/Artemis/02_expression')
options(stringsAsFactors = F)
library(stringr)

# ======================= for trans_id =======================
all_trans <- scan('all_trans', what = 'characters')
result <- data.frame(id = 1:length(all_trans), 
                     name = all_trans)

write.table(result, 'trans_info.txt', row.names = F, sep = '\t', quote = F, col.names = F)


# ===================== for samples  ========================
all_samples <- read.table('all_samples', sep = '\t')[, 1]
gtex <- all_samples[str_detect(all_samples, '^SRR')]
gtex_ids <- do.call('rbind', str_split(gtex, ':'))[, 1]
gtex_sps <- do.call('rbind', str_split(gtex, ':'))[, 2]

tcga_ids <- all_samples[!str_detect(all_samples, '^SRR')]
tcga_ids <- str_replace(tcga_ids, '^Coverage_', '')
tcga_ids <- str_replace_all(tcga_ids, '[.]', '-')

tcga_info <- read.table('TCGA_sample_info', sep = '\t', header = T)
id2cancer <- paste0('TCGA_', tcga_info$SMTS)
names(id2cancer) <- tcga_info$sra_accession

# final table
result <- data.frame(id = 1:length(all_samples), 
                     accession = c(gtex_ids, tcga_ids), 
                     info = c(gtex_sps, id2cancer[tcga_ids]))

write.table(result, 'sample_info.txt', row.names = F, sep = '\t', quote = F, col.names = F)
