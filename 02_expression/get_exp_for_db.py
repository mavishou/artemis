#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2016/12/16

import argparse
from myToolKit.general_helper import *


def main(input_file, output_file):
    with open(input_file) as reader, open(output_file, 'w') as writer:
        reader.readline()
        for line in reader:
            cols = line_to_list(line)
            trans_name = cols[0]
            l_exp = [str(round(float(e), 4)) for e in cols[1:]]
            exp_out = '|'.join(l_exp)
            out_line = [trans_name, exp_out]
            writer.write(get_output_line(out_line))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file')
    parser.add_argument('output_file')
    args = parser.parse_args()

    main(args.input_file, args.output_file)
