.mode tabs

-- table for all samples
DROP TABLE IF EXISTS samples;
CREATE TABLE samples
            (accession CHAR(100) PRIMARY KEY NOT NULL,
            desc CHAR(100) NOT NULL);
.import db_sample_info.txt samples

-- table for trans
DROP TABLE IF EXISTS annotations;
CREATE TABLE annotations
(
gene_id CHAR(50) NOT NULL,
gene_name CHAR(50) NOT NULL,
trans_id CHAR(50) PRIMARY KEY NOT NULL,
trans_name CHAR(50) NOT NULL
);
.import /lustre/user/houm/projects/Artemis/01_gene_model/db_JS_anno_maps annotations
CREATE INDEX index_of_gene_id on annotations (gene_id);
CREATE INDEX index_of_gene_name on annotations (gene_name);

-- table for expression
CREATE TABLE exps
            (trans_id CHAR(50) PRIMARY KEY NOT NULL,
            exp TEXT NOT NULL
            );
.import exp_for_db.txt exps