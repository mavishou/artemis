#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2016/12/18

import argparse
from myToolKit.general_helper import *
import pandas as pd


def read_exprs(file_name):
    df_exprs = pd.read_csv(file_name, sep='\t').iloc[:, 2:]
    return df_exprs


class CalCorr(object):

    def __init__(self, file_1, file_2):
        self.df_exprs_1 = read_exprs(file_1)
        self.df_exprs_2 = read_exprs(file_2)
        self.df_corr = None

    def cal_corr(self, method):
        df_exprs = pd.concat([self.df_exprs_1.reset_index(drop=True), self.df_exprs_2.reset_index(drop=True)], axis=1)
        df_corrs_all = df_exprs.corr(method=method)
        self.df_corr = df_corrs_all.loc[self.df_exprs_1.columns, self.df_exprs_2.columns]

    def output(self):
        self.df_corr.to_csv(sys.stdout, sep='\t')


def main(file_1, file_2, spearman):
    if spearman:
        method = 'spearman'
    else:
        method = 'pearson'

    cal_corr = CalCorr(file_1, file_2)
    cal_corr.cal_corr(method)
    cal_corr.output()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f1', '--file-1', required=True)
    parser.add_argument('-f2', '--file-2', required=True)
    parser.add_argument('-s', '--spearman', action='store_true')
    args = parser.parse_args()

    main(args.file_1, args.file_2, args.spearman)
