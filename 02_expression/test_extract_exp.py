#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2016/12/17


import unittest
import extract_exp
from extract_exp import DB


class TestDB(unittest.TestCase):

    def test_get_exp_from_db(self):
        with DB() as db:
            exps = db.get_exp('MSTRG.1.1')
            print len(exps)

    def test_get_trans_id(self):
        with DB() as db:
            db.get_all_trans('ENSG00000167578.16')

    def test_get_sample_info(self):
        with DB() as db:
            sample_info = db.get_sample_info()
            print sample_info.head()

if __name__ == '__main__':
    unittest.main()
