#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2016/12/17

import argparse
from myToolKit.general_helper import *
import sqlite3
import pandas as pd


class DB(object):

    def __init__(self):
        self.conn = sqlite3.connect('/gpfs/user/houm/Artemis/JS_exp.sqlite3')
        # self.conn = sqlite3.connect('test.sqlite3')

    def __enter__(self):
        return self

    def get_exp(self, trans):
        sql = "select exp from exps where trans_id='{}'".format(trans)
        cursor = self.conn.execute(sql)
        for row in cursor:
            exp_text = row[0]
            # print row[0]
            exps = exp_text.split('|')
            exps = [float(e) for e in exps]
            return exps

    def get_all_trans(self, gene_id):
        logging.info('Gene ID: {}'.format(gene_id))
        sql = "select trans_id from annotations where gene_id='{}'".format(gene_id)
        all_trans = pd.read_sql(sql, self.conn)
        all_trans = set(all_trans.loc[:, 'trans_id'].tolist())
        logging.info('{} transcripts: {}'.format(len(all_trans), all_trans))
        return all_trans

    def get_sample_info(self):
        sql = "select * from samples"
        sample_info = pd.read_sql(sql, self.conn)
        return sample_info

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.close()


class ProcessExp(object):

    def __init__(self, name, name_type, output_dir, db):
        self.name = name
        self.name_type = name_type
        self.output_dir = output_dir
        self.db = db
        # prepare transcripts
        self.transcripts = set()
        if name_type == 'trans':
            self.transcripts.add(name)
        elif name_type == 'gene':
            self.transcripts = db.get_all_trans(name)
        # prepare variables
        self.df_exps = None
        self.df_out_exp = None
        self.sample_info = db.get_sample_info()
        # prepare output
        prepare_dir(output_dir)
        self.out_exp = os.path.join(output_dir, 'exp.txt')
        self.out_median = os.path.join(output_dir, 'median.txt')
        self.out_mean = os.path.join(output_dir, 'mean.txt')

    def get_all_exp(self):
        d_exps = {}
        for trans in self.transcripts:
            d_exps[trans] = self.db.get_exp(trans)
        self.df_exps = pd.DataFrame(d_exps)
        if self.name_type == 'gene':
            self.df_exps.loc[:, self.name] = self.df_exps.sum(1)
        self.__post_process_for_exp()

    def __post_process_for_exp(self):
        self.df_exps.index = self.sample_info['accession']
        self.df_exp_out = self.sample_info.loc[:, ['desc', 'accession']]
        self.df_exp_out = pd.concat([self.df_exp_out.reset_index(drop=True), self.df_exps.reset_index(drop=True)], axis=1)
        self.df_exp_out.to_csv(self.out_exp, sep='\t', encoding='UTF-8', index=False)

    def calculate_for_group(self):
        df_median = self.df_exp_out.groupby('desc').median()
        df_median.to_csv(self.out_median, sep='\t', encoding='UTF-8')
        df_mean = self.df_exp_out.groupby('desc').mean()
        df_mean.to_csv(self.out_mean, sep='\t', encoding='UTF-8')


def main(name, name_type, output_dir):
    with DB() as db:
        process_exp = ProcessExp(name, name_type, output_dir, db)
        process_exp.get_all_exp()
        process_exp.calculate_for_group()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('name')
    parser.add_argument('-o', '--output_dir', required=True)
    parser.add_argument('-t', '--transcript', action='store_true')
    parser.add_argument('-g', '--gene', action='store_true')
    args = parser.parse_args()

    if args.gene:
        args.name_type = 'gene'
    else:
        args.name_type = 'trans'

    main(args.name, args.name_type, args.output_dir)
