#!/bin/bash
# Run on HPC

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1
CPU_NUM=10

prepare_target_dir() {
    TARGET_DIR=stringtie/$ID
    mkdir -p $TARGET_DIR
}

prepare_stringtie_vars() {
    BAM=alignment_hg38/$ID/$ID.sort.bam
    OUT_GTF=$TARGET_DIR/stringtie.gtf
    REF_GTF=../01_gene_model/artemis_exon.gtf
}

make_stringtie_cmd() {
    CMD="~/users/houm/tools/stringtie-1.3.1c.Linux_x86_64/stringtie $BAM"
    # CMD="$CMD -v"
    CMD="$CMD -o $OUT_GTF"
    CMD="$CMD -p $CPU_NUM"
    CMD="$CMD -G $REF_GTF"
    CMD="$CMD -A $TARGET_DIR/gene_abund.tab"
    # CMD="$CMD -C $TARGET_DIR/cov_refs.gtf"
    CMD="$CMD -B"
    CMD="$CMD -e"
}

run_stringtie() {
    echo $ID
    prepare_target_dir
    prepare_stringtie_vars
    make_stringtie_cmd
    run_cmd "$CMD" "Running stringtie"
}

run_stringtie
