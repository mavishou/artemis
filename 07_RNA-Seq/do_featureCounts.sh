#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1
CPU_NUM=5

prepare() {
    BAM_FILE=alignment_hg38/$ID/$ID.sort.bam
    TARGET_DIR=fc/$ID
    mkdir -p $TARGET_DIR
}

make_fc_cmd() {
    local gene_model_file=../01_gene_model/artemis_exon.gtf

    CMD_FC="~/users/houm/tools/subread-1.5.1-Linux-x86_64/bin/featureCounts"
    CMD_FC="$CMD_FC -p" # paired
    CMD_FC="$CMD_FC -O"
    CMD_FC="$CMD_FC -T $CPU_NUM"
    CMD_FC="$CMD_FC -t exon"
    CMD_FC="$CMD_FC -g gene_id"
    CMD_FC="$CMD_FC -a $gene_model_file"
    CMD_FC="$CMD_FC -o $TARGET_DIR/gene_reads_count"
    CMD_FC="$CMD_FC -M" # count multiple reading reads
    CMD_FC="$CMD_FC $BAM_FILE"
}

run_featureCounts() {
    make_fc_cmd
    run_cmd "$CMD_FC" "Running featureCounts"
}

main() {
    echo $ID
    prepare
    run_featureCounts
}

main
