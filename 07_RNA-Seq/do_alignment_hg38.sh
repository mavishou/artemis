#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1
SRRS=$2
CPU_NUM=5

get_fastq() {
    local srrs=$1
    local tp=$2
    local fastq_files=$(echo $srrs | tr "," "\n" | sed 's:^:../00_samples/fastq/:' | sed "s/$/_$tp.fastq.bz2/" | tr "\n" "," | sed 's/,$//')
    echo $fastq_files
}

prepare_target_dir() {
    TARGET_DIR=alignment_hg38/$ID
    mkdir -p $TARGET_DIR
}

prepare_hisat() {
    GENOME=/home/gaog_pkuhpc/users/houm/genome/human/hg38/genome
    SPLICE_FILE=../01_gene_model/JS_splice_sites.txt
    OUT_SAM=$TARGET_DIR/$ID.sam
    FQ_FILE_1=$(get_fastq $SRRS 1)
    FQ_FILE_2=$(get_fastq $SRRS 2)

}

make_hisat_cmd() {
    CMD_HISAT="~/users/houm/tools/hisat2-2.0.5/hisat2"
    CMD_HISAT="$CMD_HISAT -p $CPU_NUM"
    CMD_HISAT="$CMD_HISAT -x $GENOME"
    CMD_HISAT="$CMD_HISAT --known-splicesite-infile $SPLICE_FILE"
    CMD_HISAT="$CMD_HISAT --dta"
    CMD_HISAT="$CMD_HISAT -S $OUT_SAM"
    CMD_HISAT="$CMD_HISAT -1 $FQ_FILE_1"
    CMD_HISAT="$CMD_HISAT -2 $FQ_FILE_2"
}

prepare_convert() {
    OUT_BAM=$TARGET_DIR/$ID.bam
}

make_convert_cmd() {
    CMD_CONVERT="~/users/houm/tools/samtools-1.3.1/bin/samtools view"
    CMD_CONVERT="$CMD_CONVERT -@ $CPU_NUM"
    CMD_CONVERT="$CMD_CONVERT -b $OUT_SAM"
    CMD_CONVERT="$CMD_CONVERT > $OUT_BAM"
}

prepare_sort() {
    OUT_BAM_SORT=$TARGET_DIR/$ID.sort.bam
}

make_sort_cmd() {
    CMD_SORT="~/users/houm/tools/samtools-1.3.1/bin/samtools sort"
    CMD_SORT="$CMD_SORT -@ $CPU_NUM"
    CMD_SORT="$CMD_SORT -o $OUT_BAM_SORT"
    CMD_SORT="$CMD_SORT $OUT_BAM"
}

run_hisat() {
    prepare_hisat
    make_hisat_cmd
    run_cmd "$CMD_HISAT" "Running hisat"
}

remove_tmp_sam() {
    if [ -s $OUT_BAM ]; then rm -f $OUT_SAM; fi
}

run_convert() {
    prepare_convert
    make_convert_cmd
    run_cmd "$CMD_CONVERT" "Converting sam to bam"
    remove_tmp_sam
}

remove_tmp_bam() {
    if [ -s $OUT_BAM_SORT ]; then rm -f $OUT_BAM; fi
}


run_sort() {
    prepare_sort
    make_sort_cmd
    run_cmd "$CMD_SORT" "Sorting bam file"
    remove_tmp_bam
}

build_bam_index() {
    TARGET_BAM=$OUT_BAM_SORT
    if [[ ! -s $TARGET_BAM ]]; then
        echo "$TARGET_BAM is not existed."
        exit 1
    fi
    local cmd="~/users/houm/tools/samtools-1.3.1/bin/samtools index $TARGET_BAM"
    run_cmd "$cmd" "Building index"
}

main() {
    echo $ID
    prepare_target_dir
    run_hisat
    run_convert
    run_sort
    build_bam_index
    echo "Done!"
}

main
