#!/bin/bash
# Run on HPC

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

ID=$1
CPU_NUM=5

prepare_target_dir() {
    TARGET_DIR=stringtie_result/$ID
    mkdir -p $TARGET_DIR
}

prepare_stringtie_vars() {
    BAM=/lustre1/gaog_pkuhpc/users/kel/function_annotation_new/result/human_cell_line/hisat_result/$ID-hg38-hisat/accepted_hits.bam
    OUT_GTF=$TARGET_DIR/out.gtf
    REF_GTF=../01_gene_model/JS_exon_simple.gtf
}

make_stringtie_cmd() {
    CMD="stringtie $BAM"
    # CMD="$CMD -v"
    CMD="$CMD -o $OUT_GTF"
    CMD="$CMD -p $CPU_NUM"
    CMD="$CMD -G $REF_GTF"
    # CMD="$CMD -A $TARGET_DIR/gene_abund.tab"
    # CMD="$CMD -C $TARGET_DIR/cov_refs.gtf"
    CMD="$CMD -B"
    CMD="$CMD -e"
}

run_stringtie() {
    echo $ID
    prepare_target_dir
    prepare_stringtie_vars
    make_stringtie_cmd
    run_cmd "$CMD" "Running stringtie"
}

run_stringtie
