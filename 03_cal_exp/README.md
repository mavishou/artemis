The expression of the 4 cell lines on JS's gene model

# calculate expression
```bash
# On HPC
# cat sample_list.txt | awk '{print $1}' | while read id; do cle -s /home/gaog_pkuhpc/users/kel/function_annotation_new/result/human_cell_line/stringtie_result/${id}-hg38-stringtie stringtie_result/$id; done

cat sample_info.txt | awk '{print $1}' | hpc_batch -s call_exp.sh -x stt -c 5

# On Solar, get the result back
scp -r gaog_hm@10.100.1.88:/10.100.1.5/gaog_pkuhpc/home/gaog_pkuhpc/users/houm/Artemis/03_cal_exp/stringtie_result .
```

# Clean expression
See `clean_exp.R` to get expressed lncRNAs.

