#!/usr/bin/env bash

set -o errexit
set -o nounset

download() {
    wget $URL
}

uncompress() {
    gunzip $CLEAN_NAME  
}

rename() {
    local file_name=$(basename $URL)
    CLEAN_NAME=${CELL_TYPE}_${TYPE}_hg19.bed.gz
    mv $file_name $CLEAN_NAME
}

main() {
    while read CELL_TYPE TYPE URL; do
        echo $CELL_TYPE $TYPE
        download
        rename
        uncompress
        echo "======================================"
    done
}

main
