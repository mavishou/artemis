#!/usr/bin/env bash

set -o nounset
set -o errexit
source ~/users/houm/bin/general_func.sh

SRA=$1

prepare_target_dir() {
    TARGET_DIR=fastq
    mkdir -p $TARGET_DIR
}

do_fastq_dump() {
   local cmd="fastq-dump --bzip2 --split-3 -O $TARGET_DIR $SRA"
   echo $cmd
   run_cmd "$cmd" "Running fastq_dump"
}

main(){
    prepare_target_dir
    do_fastq_dump
}

main
