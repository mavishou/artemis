#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2017/1/11

from myToolKit.general_helper import *
import argparse
import commands
import xmltodict
import time


def get_download_url(srr_id):
    url = 'anonftp@ftp.ncbi.nlm.nih.gov:/sra/sra-instant/reads/ByRun/sra/SRR/{}/{}/{}.sra'.format(srr_id[:6], srr_id, srr_id)
    return url


class Convert(object):
    all_query_ids = []
    has_gsm_ids = []
    time_to_run = 0

    def __init__(self, query_ids, mode, prefix):
        self.query_ids = query_ids
        Convert.all_query_ids.extend(query_ids)
        self.mode = mode

        Convert.time_to_run += 1
        map_file = '{}_ID_map'.format(prefix)
        url_file = '{}_url'.format(prefix)
        check_file = '{}_title'.format(prefix)

        if Convert.time_to_run == 1:
            remove_a_file(map_file)
            remove_a_file(url_file)
            remove_a_file(check_file)

        if self.mode == 'simple':
            self.writer = sys.stdout
        else:
            self.writer = open(map_file, 'aw')
        if self.mode == 'full':
            self.writer_url = open(url_file, 'aw')
        if self.mode == 'check':
            self.writer_check = open(check_file, 'aw')
        self.do_convert()

    def _fetch(self):
        cmd = 'esearch -db sra -query "{}" | efetch -format docsum'.format(' OR '.join(self.query_ids))
        # print cmd
        run_status, output = commands.getstatusoutput(cmd)
        # print output
        if run_status == 0:
            self.fetched_results = output
        else:
            logging.warn('efetch Error!')

    def _parse(self):
        doc = xmltodict.parse(self.fetched_results)
        doc_summaries = doc['DocumentSummarySet']['DocumentSummary']
        if not isinstance(doc_summaries, list):
            doc_summaries = [doc_summaries]

        for doc_summary in doc_summaries:
            exp_xml = doc_summary['ExpXml']
            title = exp_xml['Summary']['Title']
            submitter_id = exp_xml['Submitter']['@acc']
            experiment_id = exp_xml['Experiment']['@acc']
            gsm_id = exp_xml['Experiment']['@name'].split(': ')[0]
            if gsm_id.startswith('GSM'):
                Convert.has_gsm_ids.append(gsm_id)
            elif len(self.query_ids) == 1:
                gsm_id = self.query_ids[0]
            else:
                continue
            study_id = exp_xml['Study']['@acc']
            sample_id = exp_xml['Sample']['@acc']
            runs = doc_summary['Runs']['Run']
            if not isinstance(runs, list):
                runs = [runs]
            for run in runs:
                run_id = run['@acc']
                total_base = run['@total_bases']
                self.writer.write(get_output_line([submitter_id, experiment_id, gsm_id, study_id, sample_id, run_id, total_base]))
                if self.mode == 'full':
                    url = get_download_url(run_id)
                    self.writer_url.write(url + '\n')
            if self.mode == 'check':
                self.writer_check.write(get_output_line([gsm_id, title]))

    def do_convert(self):
        n = 0
        REPEAT_NUM = 3
        while n <= REPEAT_NUM:
            n += 1
            try:
                self._fetch()
                self._parse()
                break
            except Exception:
                pass


def main(mode, prefix):
    query_ids = []
    n = 0
    MAX_NUM_QUERY = 100
    for line in sys.stdin:
        n += 1
        query_ids.append(line.rstrip('\n'))
        if n >= MAX_NUM_QUERY:
            Convert(query_ids, mode, prefix)
            # time.sleep(0.5)
            n = 0
            query_ids = []

    if len(query_ids) != 0:
        Convert(query_ids, mode, prefix)

    rerun_ids = set(Convert.all_query_ids) - set(Convert.has_gsm_ids)
    for id in rerun_ids:
        Convert([id], mode, prefix)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mode', default='simple', choices=['simple', 'full', 'check'])
    parser.add_argument('-p', '--prefix', default=None)
    args = parser.parse_args()
    if (args.mode == 'full' or args.mode == 'check') and args.prefix is None:
        parser.error('For "full" or "check" mode, you must set the prefix of output file')
    main(args.mode, args.prefix)
