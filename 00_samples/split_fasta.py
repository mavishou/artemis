#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 2017/1/20

from myToolKit.general_helper import *
from myToolKit.util.decorators import print_func_name
import argparse
import bz2
import random


class SplitFasta(object):

    def __init__(self, n_samples, subfix, input_file):
        self.n_samples = n_samples
        self.input_file = input_file
        self.base_name = input_file.replace('.fastq.bz2', '')
        self.subfix = subfix
        self.file_line_num = 0
        self.sub_index = {}
        logging.info('Number of sub samples: {}'.format(n_samples))

    @print_func_name
    def count_total_reads_num(self):
        with bz2.BZ2File(self.input_file) as reader:
            for line in reader:
                if not line.startswith('\n'):
                    self.file_line_num += 1
        logging.info('Total file lines: {}'.format(self.file_line_num))
        if self.file_line_num % 4 != 0:
            raise Exception('Line number is not multiple of 4!')

    @print_func_name
    def do_sampling(self):
        total_reads = self.file_line_num / 4
        logging.info('Total reads: {}'.format(total_reads))
        sub_reads_num = total_reads / self.n_samples
        logging.info('Reads in each samples: {}'.format(sub_reads_num))

        all = set(range(1, total_reads + 1))
        for i in range(1, self.n_samples):
            extracted = random.sample(all, sub_reads_num)
            for j in extracted:
                self.sub_index[j] = i
            all -= set(extracted)
        # the last sample
        for j in all:
            self.sub_index[j] = self.n_samples

    @print_func_name
    def do_split(self):
        with bz2.BZ2File(self.input_file) as reader:
            writers = {}
            for i in range(1, self.n_samples + 1):
                writers[i] = bz2.BZ2File('{}_{}{}.fastq.bz2'.format(self.base_name, self.subfix, i), 'w')
            n = 0
            for line in reader:
                n += 1
                if n % 4 == 1:
                    seq_num = (n + 3) / 4
                    current_writer = writers[self.sub_index[seq_num]]
                current_writer.write(line)


def main(n_samples, subifx, input_file):
    split_fasta = SplitFasta(n_samples, subifx, input_file)
    split_fasta.count_total_reads_num()
    split_fasta.do_sampling()
    split_fasta.do_split()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--n-samples', default=2, type=int)
    parser.add_argument('-s', '--subfix', default='sub')
    parser.add_argument('input_file')
    args = parser.parse_args()
    main(args.n_samples, args.subfix, args.input_file)
