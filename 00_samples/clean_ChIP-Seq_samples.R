setwd('/lustre/user/houm/projects/Artemis/00_samples')
options(stringsAsFactors = F)

# check

manual <- read.table('chip_main_manual')
titles <- read.table('chip_main_title', sep = '\t')

for_check <- merge(manual, titles, by.x="V3", by.y="V1")
write.table(for_check, file = 'chip_main_for_check.txt', sep = '\t', quote = F, row.names = F, col.names = F)

full <- read.table('chip_main_ID_map')[, c(3, 6)]

final <- merge(manual, full, by.x="V3", by.y="V3")
final %<>% arrange(V1, V2)
final[6:7, 2] <- 'GM12878'

gsm_count <- tapply(final$V6, as.factor(final$V3), length)
gsm_rep <- NULL
for(g in unique(final$V3)) {
  gsm_rep <- c(gsm_rep, 1:gsm_count[g])
}

final %<>% mutate(serial=gsm_rep)
colnames(final) <- c('GSM', 'cell_line', 'antibody', 'SRR', 'serial')

final %<>% mutate(ID=paste(GSM, serial, sep = '_'))

srrs <- tapply(final$SRR, final$GSM, function(x) paste(sort(x), collapse = ','))

final %<>% mutate(srrs = srrs[GSM])

write.table(final, 'for_get_chip_mapping.txt', sep = '\t', quote = F, row.names = F, col.names = F)
