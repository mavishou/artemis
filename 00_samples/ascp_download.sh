#!/usr/bin/env bash

set -o nounset
set -o errexit

PREFIX=$1

prepare() {
    DIR=sra/$PREFIX
    mkdir -p $DIR
}

main() {
    prepare
    while read URL; do
        ascp -k 1 -T -i /rd/build/aspera/connect/etc/asperaweb_id_dsa.openssh $URL $DIR
    done
}

main
