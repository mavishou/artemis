# Investigate LUNAR1
See ~~`investigate_cases.md`~~. Now the file is `10_LUNAR1/LUNAR1.md`

At first, I want start with known cases. The best case is **LUNAR1**. But the expression of LUNAR1 is not correlated with neighbor genes in my data. And it's hard to find helpful results in the Hi-C tool Juicebox. 

Then I try to figure out whether the LUNAR1 - IGF1R pair can be selected from the datasets related to the original paper. The answer is yes.

# Find regulatory lncRNAs directly
## Investigation
See `investigate_2.md`
I decide to find these kind of lncRNAs: it can influence the interaction of nearby promoters and enhancers, thus regulate nearby genes.

## 2017-01-25
Most process has already been determined. See `20_find_cis/find_cis.md`.


